<?php
declare(strict_types=1);

namespace Assessment\Test\Fixture;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use RestApi\TestSuite\Fixture\RestApiFixture;

class QuestionnairesFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.Questionnaires';
    const QUESTIONNAIRE_ID = 1;

    public $records = [
        [
            'id' => self::QUESTIONNAIRE_ID,
            'title' => 'Onboarding questions',
            'slug' => 'onboarding',
            'selector_text' => null,
            'language' => Languages::ENG,
            'type' => null,
            'icon' => null,
            'related_questionnaire_id' => null,
            'signup_description' => 'After signup you can blabla',
            'pdf_description_1' => 'this is the pdf one',
            'pdf_description_2' => 'this is the pdf two',
            'pdf_image' => 'www.test.com/test_image.com',
            'level_1' => 'level one text',
            'level_2' => 'level two text',
            'level_3' => 'level three text',
            'level_4' => 'level four text',
            'user_fields_component' => 'bla',
            'hide_user_fields' => true,
            'user_submission_component' => 'blabla',
            'single_submit' => false,
            'checkbox_label' => 'check box label text',
            'email_label' => 'email_label text',
            'question_info' => 'question_info text',
            'user_info' => 'user_info text',
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 2,
            'title' => 'General Questionnaire',
            'slug' => 'general-questionnaire',
            'selector_text' => null,
            'language' => Languages::ENG,
            'type' => null,
            'icon' => null,
            'related_questionnaire_id' => null,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 3,
            'title' => 'استبيان',
            'slug' => 'ar-questionnaire',
            'selector_text' => null,
            'language' => FieldsFixture::ARB,
            'type' => null,
            'icon' => null,
            'related_questionnaire_id' => null,
            'signup_description' => null,
            'pdf_description_1' => null,
            'pdf_description_2' => null,
            'pdf_image' => null,
            'level_1' => null,
            'level_2' => null,
            'level_3' => null,
            'level_4' => null,
            'created' => '2022-03-01 10:00:00',
            'modified' => '2022-03-01 10:00:00',
            'deleted' => null,
        ],
    ];
}

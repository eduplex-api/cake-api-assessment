<?php
declare(strict_types=1);

namespace Assessment\Test\Fixture;

use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\Fixture\RestApiFixture;

class SubmissionsFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.Submissions';
    const SUBMISSION_ID = 1;

    public $records = [
        [
            'id' => self::SUBMISSION_ID,
            'user_id' => UsersFixture::BUYER_ID,
            'job_position' => 'Director',
            'occupation' => 'ESCO director',
            'working_times' => 'Full time',
            'home_office' => true,
            'location' => 'Vigo',
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'rating' => 5,
            'review' => 'Love it',
            'created' => '2023-03-02 10:00:00',
            'modified' => '2023-03-02 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 2,
            'user_id' => UsersFixture::BUYER_ID,
            'job_position' => 'Manager',
            'occupation' => 'ESCO Manager',
            'working_times' => 'Part time',
            'home_office' => false,
            'location' => 'Austria',
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'rating' => 4,
            'review' => 'Like it',
            'created' => '2022-03-01 11:00:00',
            'modified' => '2022-03-01 11:00:00',
            'deleted' => null,
        ],
    ];
}

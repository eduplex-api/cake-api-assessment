<?php
declare(strict_types=1);

namespace Assessment\Test\Fixture;

use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\Fixture\RestApiFixture;

class InitiativesUsersFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.InitiativesUsers';
    const INITIATIVES_USERS_ID = 1;

    public $records = [
        [
            'id' => self::INITIATIVES_USERS_ID,
            'initiative_id' => InitiativesFixture::INITIATIVE_ID,
            'user_id' => UsersFixture::BUYER_ID,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'manager_id' => null,
            'submission_id' => 2,
            'score' => 100,
            'max_score' => 100,
            'percentage' => 100,
            'state' => null,
            'is_flagged' => false,
            'created' => '2021-03-09 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 2,
            'initiative_id' => 2,
            'user_id' => UsersFixture::BUYER_ID,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'manager_id' => null,
            'submission_id' => 2,
            'score' => 50,
            'max_score' => 100,
            'percentage' => 50,
            'state' => null,
            'is_flagged' => true,
            'created' => '2021-03-09 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
    ];
}

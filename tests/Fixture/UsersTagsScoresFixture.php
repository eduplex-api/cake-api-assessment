<?php
declare(strict_types=1);

namespace Assessment\Test\Fixture;

use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\Fixture\RestApiFixture;

class UsersTagsScoresFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.UsersTagsScores';
    const USERS_TAGS_SCORES_ID = 10;
    const TAG_SLUG = 'SCRUM';

    public $records = [
        [
            'id' => self::USERS_TAGS_SCORES_ID,
            'user_id' => UsersFixture::BUYER_ID,
            'tag' => 'Agile methodologies',
            'score' => 10,
            'max_score' => 40,
            'created' => '2024-02-19 10:00:00',
            'modified' => '2024-02-19 11:00:00',
            'deleted' => null,
        ],
        [
            'id' => 11,
            'user_id' => UsersFixture::BUYER_ID,
            'tag' => self::TAG_SLUG,
            'score' => 20,
            'max_score' => 40,
            'created' => '2024-02-01 10:00:00',
            'modified' => '2024-02-19 12:00:00',
            'deleted' => null,
        ],
    ];
}

<?php
declare(strict_types=1);

namespace Assessment\Test\Fixture;

use App\Test\Fixture\UsersFixture;
use RestApi\TestSuite\Fixture\RestApiFixture;

class JobRecommendationsFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.JobRecommendations';
    const JOB_RECOMMENDATION_ID = 1;

    public $records = [
        [
            'id' => self::JOB_RECOMMENDATION_ID,
            'user_id' => UsersFixture::BUYER_ID,
            'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
            'rating' => 3,
            'category' => 'panic',
            'distance' => 3.0,
            'created' => '2023-10-10 10:00:00',
            'modified' => '2023-10-10 11:00:00',
            'deleted' => null,
        ],
    ];
}

<?php

declare(strict_types = 1);

namespace Assessment\Test\Fixture;

use RestApi\TestSuite\Fixture\RestApiFixture;

class InitiativesTagsFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.InitiativesTags';
    const INITIATIVES_TAGS_ID = 1;

    public $records = [
        [
            'id' => self::INITIATIVES_TAGS_ID,
            'initiative_id' => InitiativesFixture::INITIATIVE_ID,
            'tag' => 'Agile methodologies'
        ]
    ];
}

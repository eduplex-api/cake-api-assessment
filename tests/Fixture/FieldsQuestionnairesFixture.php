<?php

declare(strict_types = 1);

namespace Assessment\Test\Fixture;

use RestApi\TestSuite\Fixture\RestApiFixture;

class FieldsQuestionnairesFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.FieldsQuestionnaires';

    public $records = [
        [
            'id' => 1,
            'field_id' => FieldsFixture::FIELD_ID,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID
        ],
        [
            'id' => 2,
            'field_id' => 4,
            'questionnaire_id' => 2
        ],
        [
            'id' => 3,
            'field_id' => 4,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID
        ]
    ];
}

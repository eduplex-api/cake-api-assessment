<?php

declare(strict_types = 1);

namespace Assessment\Test\Fixture;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use RestApi\TestSuite\Fixture\RestApiFixture;

class InitiativesFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.Initiatives';
    const INITIATIVE_ID = 1;

    public $records = [
        [
            'id' => self::INITIATIVE_ID,
            'title' => 'SCRUM 101 Course',
            'description' => 'Description of the course',
            'language' => Languages::ENG,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 2,
            'title' => 'Proyect management master of the universe',
            'description' => 'Blablabla',
            'language' => Languages::ENG,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 3,
            'title' => 'هذا سؤال اختباري',
            'language' => FieldsFixture::ARB,
            'description' => 'Blabla',
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
    ];
}

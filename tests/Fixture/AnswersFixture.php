<?php

declare(strict_types = 1);

namespace Assessment\Test\Fixture;

use RestApi\TestSuite\Fixture\RestApiFixture;

class AnswersFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.Answers';
    const ANSWER_ID = 1;

    public $records = [
        [
            'id' => self::ANSWER_ID,
            'question_id' => 1,
            'answer' => 'Yes, always',
            'weight' => 2,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 2,
            'question_id' => 1,
            'answer' => 'Yes, sometimes when it is necessary',
            'weight' => 1,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 3,
            'question_id' => 1,
            'answer' => 'No, never',
            'weight' => 0,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 4,
            'question_id' => 2,
            'answer' => 'Yes, TDD, Code review, BPF, etc',
            'weight' => 2,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 5,
            'question_id' => 2,
            'answer' => 'Yes, we test',
            'weight' => 1,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 6,
            'question_id' => 2,
            'answer' => 'No, that is a waste of time',
            'weight' => 0,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
    ];
}

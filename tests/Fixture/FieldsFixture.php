<?php

declare(strict_types = 1);

namespace Assessment\Test\Fixture;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use RestApi\TestSuite\Fixture\RestApiFixture;

class FieldsFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.Fields';
    const FIELD_ID = 1;
    const ARB = 'ar_SA';

    public $records = [
        [
            'id' => self::FIELD_ID,
            'title' => 'Core',
            'slug' => 'core',
            'icon' => 'atom',
            'language' => Languages::ENG,
            'position' => 1,
        ],
        [
            'id' => 2,
            'title' => 'Support',
            'slug' => 'support',
            'icon' => 'image-filter-center-focus-weak',
            'language' => Languages::ENG,
            'position' => 3,
        ],
        [
            'id' => 3,
            'title' => 'Strategic',
            'slug' => 'strategic',
            'icon' => 'cog-transfer-outline',
            'language' => Languages::ENG,
            'position' => 2,
        ],
        [
            'id' => 4,
            'title' => 'الوظائف الأساسية',
            'slug' => 'support',
            'icon' => 'image-filter-center-focus-weak',
            'language' => FieldsFixture::ARB,
        ],
    ];
}

<?php
declare(strict_types=1);

namespace Assessment\Test\Fixture;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use RestApi\TestSuite\Fixture\RestApiFixture;

class QuestionsFixture extends RestApiFixture
{
    const LOAD = 'plugin.Assessment.Questions';
    const QUESTION_ID = 1;

    public $records = [
        [
            'id' => self::QUESTION_ID,
            'question' => 'Do you follow a project check list to ensure all requirements are met?',
            'tag' => 'Project Financing Analysis',
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'position' => 2,
            'alt1' => 'Alternative one',
            'alt2' => 'Alternative two',
            'language' => Languages::ENG,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 2,
            'question' => 'Do you follow best practices?',
            'tag' => 'Project Monitoring',
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'position' => 1,
            'alt1' => 'Do you code properly?',
            'alt2' => null,
            'language' => Languages::ENG,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
        [
            'id' => 3,
            'question' => 'هذا سؤال اختباري',
            'tag' => 'Test',
            'alt1' => null,
            'alt2' => null,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'position' => 1,
            'language' => FieldsFixture::ARB,
            'created' => '2021-03-01 10:00:00',
            'modified' => '2021-03-01 10:00:00',
            'deleted' => null,
        ],
    ];
}

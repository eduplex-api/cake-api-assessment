<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\View\Helper;

use App\Test\Fixture\UsersFixture;
use Assessment\Model\Table\SubmissionsTable;
use Assessment\Test\Fixture\InitiativesFixture;
use Assessment\Test\Fixture\InitiativesTagsFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Assessment\Test\Fixture\SubmissionsFixture;
use Cake\I18n\FrozenTime;
use Cake\TestSuite\TestCase;

class SubmissionsTableTest extends TestCase
{
    private $Submissions;
    protected $fixtures = [
        SubmissionsFixture::LOAD,
        InitiativesUsersFixture::LOAD,
        InitiativesFixture::LOAD,
        InitiativesTagsFixture::LOAD,
        QuestionnairesFixture::LOAD,
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->Submissions = SubmissionsTable::load();
    }

    public function testGetLastVcResults()
    {
        $res = $this->Submissions->getLastVcResults(UsersFixture::BUYER_ID)->toArray();

        $expected = [
            'id' => 2,
            'user_id' => 3,
            'job_position' => 'Manager',
            'occupation' => 'ESCO Manager',
            'working_times' => 'Part time',
            'home_office' => false,
            'location' => 'Austria',
            'questionnaire_id' => 1,
            'review' => 'Like it',
            'rating' => 4,
            'created' => new FrozenTime('2022-03-01T11:00:00.000000+0000'),
            'modified' => new FrozenTime('2022-03-01T11:00:00.000000+0000'),
            'initiatives_users' => [
                [
                    'id' => 1,
                    'initiative_id' => 1,
                    'user_id' => 3,
                    'questionnaire_id' => 1,
                    'manager_id' => null,
                    'submission_id' => 2,
                    'score' => 100,
                    'max_score' => 100,
                    'percentage' => 100,
                    'state' => null,
                    'is_flagged' => false,
                    'created' => new FrozenTime('2021-03-09T10:00:00.000000+0000'),
                    'modified' => new FrozenTime('2021-03-01T10:00:00.000000+0000'),
                    'combined_amount' => 1,
                    'initiative' => [
                        'id' => 1,
                        'title' => 'SCRUM 101 Course',
                        'description' => 'Description of the course',
                        'created' => new FrozenTime('2021-03-01T10:00:00.000000+0000'),
                        'modified' => new FrozenTime('2021-03-01T10:00:00.000000+0000'),
                        'tags_list' => ['Agile methodologies'],
                    ]
                ],
                [
                    'id' => 2,
                    'initiative_id' => 2,
                    'user_id' => 3,
                    'questionnaire_id' => 1,
                    'manager_id' => null,
                    'submission_id' => 2,
                    'score' => 50,
                    'max_score' => 100,
                    'percentage' => 50,
                    'state' => null,
                    'is_flagged' => true,
                    'created' => new FrozenTime('2021-03-09T10:00:00.000000+0000'),
                    'modified' => new FrozenTime('2021-03-01T10:00:00.000000+0000'),
                    'combined_amount' => 1,
                    'initiative' => [
                        'id' => 2,
                        'title' => 'Proyect management master of the universe',
                        'description' => 'Blablabla',
                        'created' => new FrozenTime('2021-03-01T10:00:00.000000+0000'),
                        'modified' => new FrozenTime('2021-03-01T10:00:00.000000+0000'),
                        'tags_list' => [],
                    ]
                ],
            ],
            'questionnaire' => [
                'id' => 1,
                'title' => 'Onboarding questions',
                'slug' => 'onboarding',
                'selector_text' => null,
                'language' => 'en_US',
                'type' => null,
                'icon' => null,
                'related_questionnaire_id' => null,
                'signup_description' => 'After signup you can blabla',
                'pdf_description_1' => 'this is the pdf one',
                'pdf_description_2' => 'this is the pdf two',
                'pdf_image' => 'www.test.com/test_image.com',
                'pdf_title' => null,
                'level_1' => 'level one text',
                'level_2' => 'level two text',
                'level_3' => 'level three text',
                'level_4' => 'level four text',
                'level_1_title' => null,
                'level_2_title' => null,
                'level_3_title' => null,
                'level_4_title' => null,
                'user_fields_component' => 'bla',
                'hide_user_fields' => true,
                'user_submission_component' => 'blabla',
                'single_submit' => false,
                'checkbox_label' => 'check box label text',
                'email_label' => 'email_label text',
                'question_info' => 'question_info text',
                'user_info' => 'user_info text',
                'fields' => [],
            ],
        ];
        $this->assertEquals($expected, $res);
    }
}

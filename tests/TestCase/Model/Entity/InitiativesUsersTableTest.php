<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\View\Helper;

use App\Lib\Consts\UserGroups;
use App\Model\Entity\User;
use App\Test\Fixture\UsersFixture;
use Assessment\Lib\Consts\InitiativesUsersStates;
use Assessment\Model\Entity\InitiativesTag;
use Assessment\Model\Entity\InitiativesUser;
use Assessment\Model\Table\InitiativesTagsTable;
use Assessment\Model\Table\InitiativesUsersTable;
use Assessment\Model\Table\QuestionnairesTable;
use Assessment\Test\Fixture\InitiativesFixture;
use Assessment\Test\Fixture\InitiativesTagsFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Assessment\Test\Fixture\SubmissionsFixture;
use Cake\I18n\FrozenTime;
use Cake\TestSuite\Fixture\FixtureStrategyInterface;
use Cake\TestSuite\Fixture\TruncateStrategy;
use Cake\TestSuite\TestCase;

class InitiativesUsersTableTest extends TestCase
{
    private $InitiativesUsers;
    protected $fixtures = [
        InitiativesUsersFixture::LOAD,
        InitiativesTagsFixture::LOAD,
        InitiativesFixture::LOAD,
        UsersFixture::LOAD,
        QuestionnairesFixture::LOAD
    ];

    protected function getFixtureStrategy(): FixtureStrategyInterface
    {
        return new TruncateStrategy();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->InitiativesUsers = InitiativesUsersTable::load();
    }

    public function testValidator_PatchInitiativeUserStateCompleted()
    {
        $data = ['state' => InitiativesUsersStates::COMPLETED];
        /** @var InitiativesUser $initiativeUser */
        $initiativeUser = $this->InitiativesUsers->newEmptyEntity();
        $this->InitiativesUsers->patchEntity($initiativeUser, $data);
        $this->assertEquals($initiativeUser->state, $data['state']);
    }

    public function testValidator_PatchInitiativeUserStateNull()
    {
        $data = ['state' => null];
        /** @var InitiativesUser $initiativeUser */
        $initiativeUser = $this->InitiativesUsers->newEmptyEntity();
        $this->InitiativesUsers->patchEntity($initiativeUser, $data);
        $this->assertEquals($initiativeUser->state, $data['state']);
    }

    public function testValidator_PatchInitiativeUserStateInvalid_Exception()
    {
        $data = ['state' => 'blabla'];
        /** @var InitiativesUser $initiativeUser */
        $initiativeUser = $this->InitiativesUsers->newEmptyEntity();
        $this->expectExceptionMessage('Validation Exception');
        $this->InitiativesUsers->patchEntity($initiativeUser, $data);
    }

    public function testGetScorecardSummary()
    {
        $expectedAvgI1 = 67;
        $expectedAvgI2 = 50;
        $this->_seedInitiavesUsersData();

        $res = $this->InitiativesUsers->getScorecardSummary();

        $this->assertEquals($expectedAvgI1, $res[0]['avg_percentage']);
        $this->assertEquals($expectedAvgI2, $res[1]['avg_percentage']);
        $this->assertNotNull($res[0]['initiative']['tags_list']);
    }

    public function testGetScorecardSummary_NoInitiativesForTagsList_EmptyArray()
    {
        $filters = [
            'tags' => ['bla', 'random tag']
        ];

        $res = $this->InitiativesUsers->getScorecardSummary($filters);

        $this->assertEmpty($res);
    }

    public function testGetScorecardSummaryIncludeDeleted()
    {
        $expectedAvgI1 = 67;
        $expectedAvgI2 = 50;
        $this->_seedInitiavesUsersData();
        $this->InitiativesUsers->softDelete(1);
        InitiativesTagsTable::load()->save(new InitiativesTag([
                'initiative_id' => 2,
                'tag' => 'Test'
        ]));
        $filters = [
            'historic' => 1,
            'tags' => [
                'Agile methodologies', 'Test'
            ]
        ];

        $res = $this->InitiativesUsers->getScorecardSummary($filters);

        $this->assertEquals($expectedAvgI1, $res[0]['avg_percentage']);
        $this->assertEquals($expectedAvgI2, $res[1]['avg_percentage']);
        $this->assertNotNull($res[0]['initiative']['tags_list']);
    }

    public function testGetScorecardSummary_GroupedByState()

    {
        $this->_seedInitiavesUsersForGroupedByState();
        $expected = [
            [
                'not_started' => 2,
                'in_progress' => 1,
                'completed' => 2,
                'avg_not_started' => 35.0,
                'avg_in_progress' => 50.0,
                'avg_completed' => 100.0,
                'initiative' => [
                    'id' => 1,
                    'title' => 'SCRUM 101 Course',
                    'created' => new FrozenTime('2021-03-01 10:00:00'),
                    'tags_list' => [
                        0 => 'Agile methodologies'
                    ]
                ],
            ]
        ];

        $res = $this->InitiativesUsers->getScorecardSummaryGroupedByState();

        $this->assertEquals($expected, $res);
    }

    public function testGetDashboardTopPerformers()
    {
        $expectedUsers = 3;
        $topUserScore = 95;
        $topUserFirstname = 'hundred';
        $this->_seedInitiavesUsersData();
        $this->InitiativesUsers->saveMany([
            new InitiativesUser([
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => 100,
                'score' => 100,
                'max_score' => 100,
                'percentage' => 100,
                'state' => InitiativesUsersStates::NOT_STARTED,
            ]),
            new InitiativesUser([
                'initiative_id' => 2,
                'user_id' => 100,
                'score' => 90,
                'max_score' => 100,
                'percentage' => 90,
                'state' => InitiativesUsersStates::NOT_STARTED,
            ]),
        ]);
        $now = new FrozenTime();
        $this->InitiativesUsers->Users->save(
            new User([
                'id' => 100,
                'email' => 'hundred@example.com',
                'firstname' => 'hundred',
                'lastname' => 'Cien',
                'password' => 'x',
                'group_id' => UserGroups::BUYER,
                'created' => $now,
                'modified' => $now,
            ])
        );

        $res = $this->InitiativesUsers->getDashboardTopPerformers();
        $this->assertEquals($expectedUsers, count($res));
        $this->assertEquals($topUserScore, $res[0]['avg_percentage']);
        $this->assertEquals($topUserFirstname, $res[0]['user']['firstname']);
    }

    public function testGetDashboardTotalRespondentsByState()
    {
        $this->_seedInitiavesUsersData();
        $expected = [
            [
                'state' => InitiativesUsersStates::COMPLETED,
                'amount' => 1
            ],
            [
                'state' => InitiativesUsersStates::IN_PROGRESS,
                'amount' => 1
            ],
            [
                'state' => InitiativesUsersStates::NOT_STARTED,
                'amount' => 2
            ],
        ];

        $result = $this->InitiativesUsers->getDashboardTotalRespondentsByState();

        $this->assertEquals($expected, $result);
    }

    public function testGetDashboardTotalRespondentsByScore()
    {
        $this->_seedInitiavesUsersData();
        InitiativesUsersTable::load()->updateAll(['percentage' => 10], ['id' => 2]);
        InitiativesUsersTable::load()->updateAll(['percentage' => 90], ['id' => 3]);
        $expected = [
            'range-1' => 1,
            'range-2' => 0,
            'range-3' => 3,
            'range-4' => 0,
            'range-5' => 0,
            'range-6' => 1,
            'range-7' => 1,
        ];

        $result = $this->InitiativesUsers->getDashboardTotalRespondentsByScore([]);

        $this->assertEquals($expected, $result);
    }

    public function testGetDashboardTotalRespondentsByScoreGroupedByInitiative()
    {
        $this->_seedInitiavesUsersData();
        InitiativesUsersTable::load()->updateAll(['percentage' => 10], ['id' => 2]);
        InitiativesUsersTable::load()->updateAll(['percentage' => 80], ['id' => 3]);
        $filters = ['tags' => 'Agile methodologies'];
        $expected = [
            'Agile methodologies' => [
                'range-1' => 0,
                'range-2' => 0,
                'range-3' => 1,
                'range-4' => 1,
                'range-5' => 1,
            ]
        ];

        $result = $this->InitiativesUsers
            ->getDashboardTotalRespondentsByScoreGroupedByInitiative($filters);

        $this->assertEquals($expected, $result);
    }

    public function testGetDashboard_EmptyUserIdList_InitiativesEmpty()
    {
        $filters = ['user_ids' => []];
        $expected = [
            'Agile methodologies' => [
                'range-1' => (int)0,
                'range-2' => (int)0,
                'range-3' => (int)0,
                'range-4' => (int)0,
                'range-5' => (int)0
            ]
        ];

        $result = $this->InitiativesUsers
            ->getDashboardTotalRespondentsByScoreGroupedByInitiative($filters);

        $this->assertEquals($expected, $result);
    }

    public function testGetLastInitiativesUsersByUser_QuetionnaireWithParent_AvgScores()
    {
        $userId = UsersFixture::BUYER_ID;
        $questionnaireId = QuestionnairesFixture::QUESTIONNAIRE_ID;
        $this->InitiativesUsers->updateAll(
            ['state' => InitiativesUsersStates::IN_PROGRESS], ['id' => 1]);
        QuestionnairesTable::load()->updateAll(
            ['related_questionnaire_id' => 2], ['id' => $questionnaireId]);
        $this->InitiativesUsers->saveMany([
            new InitiativesUser([
                'initiative_id' => 1,
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => 2,
                'manager_id' => null,
                'score' => 50,
                'max_score' => 100,
                'percentage' => 50,
            ]),
            new InitiativesUser([
                'initiative_id' => 2,
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => 2,
                'score' => 50,
                'max_score' => 100,
                'percentage' => 100,
            ])
        ]);

        $result = $this->InitiativesUsers
            ->getLastInitiativesUsersByUser($userId, $questionnaireId);

        $this->assertEquals(2, count($result));
        $this->assertEquals(200, $result[0]->max_score);
        $this->assertEquals(150, $result[0]->score);
        $this->assertEquals(75, $result[0]->percentage);
        $this->assertEquals(2, $result[0]->combined_amount);
        $this->assertEquals(InitiativesUsersStates::IN_PROGRESS, $result[0]->state);
        $this->assertEquals(200, $result[1]->max_score);
        $this->assertEquals(100, $result[1]->score);
        $this->assertEquals(75, $result[1]->percentage);
        $this->assertEquals(2, $result[1]->combined_amount);
    }

    public function testGetLastInitiativesUsersByUser_BySubmission_IncludeDeleted()
    {
        $userId = UsersFixture::BUYER_ID;
        $questionnaireId = QuestionnairesFixture::QUESTIONNAIRE_ID;
        $this->InitiativesUsers->updateAll([
            'submission_id' => SubmissionsFixture::SUBMISSION_ID,
            'deleted' => new FrozenTime()
        ], ['id' => 1]);
        $this->InitiativesUsers->saveMany([
            new InitiativesUser([
                'initiative_id' => 1,
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => $questionnaireId,
                'manager_id' => null,
                'submission_id' => SubmissionsFixture::SUBMISSION_ID,
                'score' => 50,
            ]),
            new InitiativesUser([
                'initiative_id' => 2,
                'user_id' => UsersFixture::BUYER_ID,
                'submission_id' => SubmissionsFixture::SUBMISSION_ID,
                'questionnaire_id' => $questionnaireId,
                'score' => 50,
            ])
        ]);
        $filter = ['submission_id' => 1];

        /** @var InitiativesUser[] $result */
        $result = $this->InitiativesUsers
            ->getLastInitiativesUsersByUser($userId, $questionnaireId, $filter);

        $this->assertEquals(3, count($result));
        $this->assertNotNull($result[0]->deleted);

    }

    public function testGetLastInitiativesUsersByUser_QuetionnaireWithParent_AvgScoresExcludingZeros()
    {
        $userId = UsersFixture::BUYER_ID;
        $questionnaireId = QuestionnairesFixture::QUESTIONNAIRE_ID;
        $this->InitiativesUsers->updateAll(
            ['state' => InitiativesUsersStates::IN_PROGRESS, 'score' => 0, 'percentage' => 0], ['id' => 1]);
        QuestionnairesTable::load()->updateAll(
            ['related_questionnaire_id' => 2], ['id' => $questionnaireId]);
        $this->InitiativesUsers->saveMany([
            new InitiativesUser([
                'initiative_id' => 1,
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => 2,
                'manager_id' => null,
                'score' => 50,
                'max_score' => 100,
                'percentage' => 50,
            ]),
            new InitiativesUser([
                'initiative_id' => 2,
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => 2,
                'score' => 0,
                'max_score' => 100,
                'percentage' => 100,
            ])
        ]);

        $result = $this->InitiativesUsers
            ->getLastInitiativesUsersByUser($userId, $questionnaireId);

        $this->assertEquals(2, count($result));
        $this->assertEquals(1, $result[0]->combined_amount);
        $this->assertEquals(100, $result[0]->max_score);
        $this->assertEquals(50, $result[0]->score);
        $this->assertEquals(50, $result[0]->percentage);
        $this->assertEquals(InitiativesUsersStates::IN_PROGRESS, $result[0]->state);
        $this->assertEquals(2, $result[1]->combined_amount);
        $this->assertEquals(200, $result[1]->max_score);
        $this->assertEquals(50, $result[1]->score);
        $this->assertEquals(75, $result[1]->percentage);
    }

    private function _seedInitiavesUsersData()
    {
        $this->InitiativesUsers->saveMany([
            new InitiativesUser([
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => 10,
                'score' => 50,
                'max_score' => 100,
                'percentage' => 50,
                'state' => InitiativesUsersStates::NOT_STARTED,
            ]),
            new InitiativesUser([
                'initiative_id' => 2,
                'user_id' => 10,
                'score' => 100,
                'max_score' => 100,
                'percentage' => 50,
                'state' => InitiativesUsersStates::NOT_STARTED,
            ]),
            new InitiativesUser([
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => 20,
                'score' => 10,
                'max_score' => 20,
                'percentage' => 50,
                'state' => InitiativesUsersStates::COMPLETED,
            ]),
            new InitiativesUser([
                'initiative_id' => 2,
                'user_id' => 20,
                'score' => 15,
                'max_score' => 30,
                'percentage' => 50,
                'state' => InitiativesUsersStates::IN_PROGRESS,
            ]),
        ]);
    }

    private function _seedInitiavesUsersForGroupedByState()
    {
        $this->InitiativesUsers->saveMany([
            new InitiativesUser([
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => 10,
                'score' => 15,
                'max_score' => 30,
                'percentage' => 50,
                'state' => InitiativesUsersStates::NOT_STARTED,
            ]),
            new InitiativesUser([
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => 11,
                'score' => 2,
                'max_score' => 10,
                'percentage' => 20,
                'state' => InitiativesUsersStates::NOT_STARTED,
            ]),
            new InitiativesUser([
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => 12,
                'score' => 30,
                'max_score' => 30,
                'percentage' => 100,
                'state' => InitiativesUsersStates::COMPLETED,
            ]),
            new InitiativesUser([
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => 13,
                'score' => 10,
                'max_score' => 10,
                'percentage' => 100,
                'state' => InitiativesUsersStates::COMPLETED,
            ]),
            new InitiativesUser([
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => 14,
                'score' => 5,
                'max_score' => 10,
                'percentage' => 50,
                'state' => InitiativesUsersStates::IN_PROGRESS,
            ]),
        ]);
    }
}

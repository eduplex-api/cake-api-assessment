<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\View\Helper;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use App\Test\Fixture\UsersFixture;
use Assessment\Model\Table\QuestionnairesTable;
use Assessment\Test\Fixture\FieldsFixture;
use Assessment\Test\Fixture\FieldsQuestionnairesFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Cake\TestSuite\Fixture\FixtureStrategyInterface;
use Cake\TestSuite\Fixture\TruncateStrategy;
use Cake\TestSuite\TestCase;

class QuestionnairesTableTest extends TestCase
{
    /** @var QuestionnairesTable $Questionnaires */
    private $Questionnaires;
    protected $fixtures = [
        FieldsFixture::LOAD,
        FieldsQuestionnairesFixture::LOAD,
        QuestionnairesFixture::LOAD,
        InitiativesUsersFixture::LOAD,
    ];

    protected function getFixtureStrategy(): FixtureStrategyInterface
    {
        return new TruncateStrategy();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->Questionnaires = QuestionnairesTable::load();
    }

    public function testFindWithFields()
    {
        $expectedQuestionnaires = [
            'id' => 1,
            'title' => 'Onboarding questions',
            'slug' => 'onboarding',
            'selector_text' => null,
            'language' => Languages::ENG,
            'type' => null,
            'icon' => null,
            'related_questionnaire_id' => null,
            'signup_description' => 'After signup you can blabla',
            'pdf_description_1' => 'this is the pdf one',
            'pdf_description_2' => 'this is the pdf two',
            'pdf_image' => 'www.test.com/test_image.com',
            'level_1' => 'level one text',
            'level_2' => 'level two text',
            'level_3' => 'level three text',
            'level_4' => 'level four text',
            'pdf_title' => null,
            'level_1_title' => null,
            'level_2_title' => null,
            'level_3_title' => null,
            'level_4_title' => null,
            'user_fields_component' => 'bla',
            'hide_user_fields' => true,
            'user_submission_component' => 'blabla',
            'single_submit' => false,
            'checkbox_label' => 'check box label text',
            'email_label' => 'email_label text',
            'question_info' => 'question_info text',
            'user_info' => 'user_info text',
            'fields' => [
                [
                    'id' => 1,
                    'title' => 'Core',
                    'slug' => 'core',
                    'icon' => 'atom',
                    'position' => 1
                ]
            ]

        ];

        $res = $this->Questionnaires->findAllWithFields()->all();

        $this->assertEquals($expectedQuestionnaires, $res->first()->toArray());
        $this->assertEquals(2, $res->count());
    }

    public function testFindAnsweredByUser_OnlyAnsweredByUser()
    {
        $expectedQuestionnaires = [
            'id' => 1,
            'title' => 'Onboarding questions',
            'slug' => 'onboarding',
            'selector_text' => null,
            'language' => Languages::ENG,
            'type' => null,
            'icon' => null,
            'signup_description' => 'After signup you can blabla',
            'pdf_description_1' => 'this is the pdf one',
            'pdf_description_2' => 'this is the pdf two',
            'pdf_image' => 'www.test.com/test_image.com',
            'level_1' => 'level one text',
            'level_2' => 'level two text',
            'level_3' => 'level three text',
            'level_4' => 'level four text',
            'pdf_title' => null,
            'level_1_title' => null,
            'level_2_title' => null,
            'level_3_title' => null,
            'level_4_title' => null,
            'user_fields_component' => 'bla',
            'hide_user_fields' => true,
            'user_submission_component' => 'blabla',
            'single_submit' => false,
            'checkbox_label' => 'check box label text',
            'email_label' => 'email_label text',
            'question_info' => 'question_info text',
            'user_info' => 'user_info text',
            'related_questionnaire_id' => null,
            'fields' => []
        ];

        $res = $this->Questionnaires->findAnsweredByUser(UsersFixture::BUYER_ID)->all();

        $this->assertEquals(1, $res->count());
        $this->assertEquals($expectedQuestionnaires, $res->first()->toArray());
    }

    public function testFindBySlugWithFields()
    {
        $slug = 'onboarding';
        $expectedQuestionnaires = [
            'id' => 1,
            'title' => 'Onboarding questions',
            'slug' => 'onboarding',
            'selector_text' => null,
            'language' => Languages::ENG,
            'type' => null,
            'icon' => null,
            'signup_description' => 'After signup you can blabla',
            'pdf_description_1' => 'this is the pdf one',
            'pdf_description_2' => 'this is the pdf two',
            'pdf_image' => 'www.test.com/test_image.com',
            'level_1' => 'level one text',
            'level_2' => 'level two text',
            'level_3' => 'level three text',
            'level_4' => 'level four text',
            'pdf_title' => null,
            'level_1_title' => null,
            'level_2_title' => null,
            'level_3_title' => null,
            'level_4_title' => null,
            'related_questionnaire_id' => null,
            'user_fields_component' => 'bla',
            'hide_user_fields' => true,
            'user_submission_component' => 'blabla',
            'single_submit' => false,
            'checkbox_label' => 'check box label text',
            'email_label' => 'email_label text',
            'question_info' => 'question_info text',
            'user_info' => 'user_info text',
            'fields' => [
                [
                    'id' => 1,
                    'title' => 'Core',
                    'slug' => 'core',
                    'icon' => 'atom',
                    'position' => 1
                ]
            ]
        ];

        $res = $this->Questionnaires->findBySlugWithFields($slug)->first();

        $this->assertEquals($expectedQuestionnaires, $res->toArray());
    }
}

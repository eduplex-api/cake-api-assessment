<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Test\Fixture\FieldsFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class FieldsControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        FieldsFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/fields/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testGetList_GetsAllFields()
    {
        $expectedFields = [
            [
                'id' => FieldsFixture::FIELD_ID,
                'title' => 'Core',
                'icon' => 'atom',
                'slug' => 'core',
                'position' => 1,
            ],
            [
                'id' => 3,
                'title' => 'Strategic',
                'slug' => 'strategic',
                'icon' => 'cog-transfer-outline',
                'position' => 2,
            ],
            [
                'id' => 2,
                'title' => 'Support',
                'slug' => 'support',
                'icon' => 'image-filter-center-focus-weak',
                'position' => 3,
            ],
        ];

        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedFields, $bodyDecoded['data']);
    }

    public function testGet_GetSingleField()
    {
        $expectedField = [
            'id' => FieldsFixture::FIELD_ID,
            'title' => 'Core',
            'icon' => 'atom',
            'slug' => 'core',
            'position' => 1
        ];

        $this->get($this->_getEndpoint() . FieldsFixture::FIELD_ID);

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedField, $bodyDecoded['data']);
    }

}

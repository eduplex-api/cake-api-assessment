<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Model\Entity\Questionnaire;
use Assessment\Model\Table\QuestionnairesTable;
use Assessment\Test\Fixture\FieldsFixture;
use Assessment\Test\Fixture\FieldsQuestionnairesFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class QuestionnairesControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        QuestionnairesFixture::LOAD,
        FieldsQuestionnairesFixture::LOAD,
        FieldsFixture::LOAD,
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        InitiativesUsersFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/questionnaires/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testGetList_GetsAllQuestionnairesWithFields()
    {
        $expectedTotal = 2;
        $expectedQuestionnaire = $this->_getExpectedQuestionnaire();

        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedTotal, count($bodyDecoded['data']));
        $this->assertEquals($expectedQuestionnaire, $bodyDecoded['data'][0]);
    }

    public function testGetList_changingLang()
    {
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN,
                'X-Experience-API-Version' => '1.0.1',
                'Accept-language' => 'de',
            ]
        ]);

        $this->get($this->_getEndpoint());

        $bodyDecoded =$this->assertJsonResponseOK();
        $this->assertEquals([], $bodyDecoded['data']);
    }

    public function testGetList_FilterByOnlyAnsweredByUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedTotal = 1;
        $expectedQuestionnaire = $this->_getExpectedQuestionnaire();
        $expectedQuestionnaire['fields'] = [];

        $this->get($this->_getEndpoint() . '?only_answered=1');

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedTotal, count($bodyDecoded['data']));
        $this->assertEquals($expectedQuestionnaire, $bodyDecoded['data'][0]);
    }

    public function testGetData_GetsQuestionnaireBySlugWithFields()
    {
        $slug = 'onboarding';
        $expectedQuestionnaire = $this->_getExpectedQuestionnaire();

        $this->get($this->_getEndpoint() . $slug);

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedQuestionnaire, $bodyDecoded['data']);
    }

    public function testGetData_GetsQuestionnaireByIdWithFields()
    {
        $expectedQuestionnaire = $this->_getExpectedQuestionnaire();

        $this->get($this->_getEndpoint() . 1);

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedQuestionnaire, $bodyDecoded['data']);
    }

    public function testAddNew_SellerToken_ForbiddenException()
    {
        $expectedException = 'Resource not allowed with this token';
        $data = [
            'slug' => 'example'
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertResponseCode(403);
        $this->assertResponseContains($expectedException);
    }

    public function testAddNew_CreatesNewQuestionnaire()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $data = [
            'title' => 'Onboarding questions',
            'slug' => 'onboarding',
            'signup_description' => 'After signup you can blabla',
            'pdf_description_1' => 'this is the pdf one',
            'pdf_description_2' => 'this is the pdf two',
            'pdf_image' => 'www.test.com/test_image.com',
            'level_1' => 'level one text',
            'level_2' => 'level two text',
            'level_3' => 'level three text',
            'level_4' => 'level four text',
            'fields_ids' => [2, 1]
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];

        $this->assertEquals($data['title'], $response['title']);
        $this->assertEquals($data['slug'], $response['slug']);
        $this->assertEquals(Languages::ENG, $response['language']);

    }

    public function testEdit_EditsQuestionnaire()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $expected = $this->_getExpectedQuestionnaire();
        $data = [
            'title' => 'title edited',
            'type' => 'BLA',
            'icon' => 'question-icon',
            'related_questionnaire_id' => 2,
            'signup_description' => 'edit blabla',
            'pdf_description_1' => 'edit one',
            'pdf_description_2' => 'edit pdf two',
            'pdf_image' => 'www.edit.com/test_image.com',
            'level_1' => '1',
            'level_2' => '2',
            'level_3' => '3',
            'level_4' => '4',
            'pdf_title' => 'the pdf',
            'level_1_title' => 'title 1',
            'level_2_title' => 'title 2',
            'level_3_title' => 'title 3',
            'level_4_title' => 'title 4',
            'fields_ids' => [2]
        ];
        $expected = array_merge($expected, $data);
        unset($expected['fields_ids']);
        $expected['fields'] = [
            [
                'id' => 2,
                'title' => 'Support',
                'slug' => 'support',
                'icon' => 'image-filter-center-focus-weak',
                'position' => 3
            ]
        ];

        $this->patch($this->_getEndpoint() . QuestionnairesFixture::QUESTIONNAIRE_ID, $data);

        $response = $this->assertJsonResponseOK()['data'];

        $this->assertEquals($expected, $response);
    }

    public function testDelete_DeletesQuestionnaire()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $questionnaireId = 1;

        $this->delete($this->_getEndpoint() . $questionnaireId);

        $this->assertResponseOk($this->_getBodyAsString());
        /** @var Questionnaire $qSaved */
        $qSaved = QuestionnairesTable::load()->findById($questionnaireId)->first();
        $this->assertNull($qSaved);
    }

    private function _getExpectedQuestionnaire(): array
    {
        $expectedQuestionnaire = [
            'id' => 1,
            'title' => 'Onboarding questions',
            'slug' => 'onboarding',
            'selector_text' => null,
            'type' => null,
            'icon' => null,
            'related_questionnaire_id' => null,
            'language' => Languages::ENG,
            'signup_description' => 'After signup you can blabla',
            'pdf_description_1' => 'this is the pdf one',
            'pdf_description_2' => 'this is the pdf two',
            'pdf_image' => 'www.test.com/test_image.com',
            'level_1' => 'level one text',
            'level_2' => 'level two text',
            'level_3' => 'level three text',
            'level_4' => 'level four text',
            'pdf_title' => null,
            'level_1_title' => null,
            'level_2_title' => null,
            'level_3_title' => null,
            'level_4_title' => null,
            'user_fields_component' => 'bla',
            'hide_user_fields' => true,
            'user_submission_component' => 'blabla',
            'single_submit' => false,
            'checkbox_label' => 'check box label text',
            'email_label' => 'email_label text',
            'question_info' => 'question_info text',
            'user_info' => 'user_info text',
            'fields' => [
                [
                    'id' => 1,
                    'title' => 'Core',
                    'slug' => 'core',
                    'icon' => 'atom',
                    'position' => 1
                ]
            ]
        ];
        return $expectedQuestionnaire;
    }
}

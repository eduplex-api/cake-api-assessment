<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Model\Table\UsersTagsScoresTable;
use Assessment\Test\Fixture\UsersTagsScoresFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class UsersTagsScoresControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD, UsersTagsScoresFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID
            . '/users_tags_scores/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testAddNew_returnsUserInitiativeUserScoreByTag()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'tag' => UsersTagsScoresFixture::TAG_SLUG,
            'score' => 30,
            'max_score' => 40];
        $expected = [
            'id' => 12,
            'user_id' => 3,
            'tag' => UsersTagsScoresFixture::TAG_SLUG,
            'score' => 30,
            'max_score' => 40,
        ];

        $this->post($this->_getEndpoint(), $data);

        $response = $this->assertJsonResponseOK()['data'];
        unset($response['created']);
        unset($response['modified']);
        $this->assertEquals($expected, $response);
        $oldScore = UsersTagsScoresTable::load()->find()->where(['id' => 11])->first();
        $this->assertNull($oldScore);

    }
}

<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Model\Table\JobRecommendationsTable;
use Assessment\Test\Fixture\JobRecommendationsFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class JobRecommendationsControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        JobRecommendationsFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID . '/job_recommendations/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testGetList_GetsAllJobRecommendationsByUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expected = $this->_getExpectedJobRecommendations();

        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    public function testGetData_GetRecommendation()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedSubmissions = $this->_getExpectedJobRecommendations();

        $this->get($this->_getEndpoint() . JobRecommendationsFixture::JOB_RECOMMENDATION_ID);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expectedSubmissions[0], $bodyDecoded['data']);
    }

    public function testEdit_EditReviewAndRatingInSubmission()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
            'rating' => 5,
            'category' => 'learn',
            'distance' => 0.2122,
        ];
        $expected = array_merge($this->_getExpectedJobRecommendations()[0], $data);

        $this->patch($this->_getEndpoint() . JobRecommendationsFixture::JOB_RECOMMENDATION_ID, $data);

        $response = $this->assertJsonResponseOK();
        unset($response['data']['modified']);
        unset($expected['modified']);
        $this->assertEquals($expected, $response['data']);
    }

    public function testAddNew_EditReviewAndRatingInSubmission()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
            'rating' => 5,
            'category' => 'learn',
            'distance' => 0.2122,
        ];
        $expected = array_merge($data, ['user_id' => UsersFixture::BUYER_ID]);

        $this->patch($this->_getEndpoint() . JobRecommendationsFixture::JOB_RECOMMENDATION_ID, $data);

        $response = $this->assertJsonResponseOK();
        unset($response['data']['id']);
        unset($response['data']['created']);
        unset($response['data']['modified']);
        $this->assertEquals($expected, $response['data']);
    }

    public function testDelete_DeleteReviewAndRatingInSubmission()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);

        $this->delete($this->_getEndpoint() . JobRecommendationsFixture::JOB_RECOMMENDATION_ID);

        $this->assertResponseOk($this->_getBodyAsString());
        $recommendation = JobRecommendationsTable::load()
            ->findById(JobRecommendationsFixture::JOB_RECOMMENDATION_ID)
            ->first();
        $this->assertNull($recommendation);
    }

    private function _getExpectedJobRecommendations(): array
    {
        return [
            [
                'id' => JobRecommendationsFixture::JOB_RECOMMENDATION_ID,
                'user_id' => UsersFixture::BUYER_ID,
                'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                'rating' => 3,
                'category' => 'panic',
                'distance' => 3.0,
                'created' => '2023-10-10T10:00:00+00:00',
                'modified' => '2023-10-10T11:00:00+00:00'
            ],
        ];
    }


}

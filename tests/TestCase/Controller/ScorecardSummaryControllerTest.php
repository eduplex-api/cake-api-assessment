<?php

declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Controller\Component\OAuthServerComponent;
use App\Model\Table\UsersTable;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Controller\ScorecardSummaryController;
use Assessment\Lib\Consts\InitiativesUsersStates;
use Assessment\Model\Table\InitiativesUsersTable;
use Assessment\Test\Fixture\InitiativesFixture;
use Assessment\Test\Fixture\InitiativesTagsFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use ReflectionClass;
use RestApi\TestSuite\ApiCommonErrorsTest;

class ScorecardSummaryControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        InitiativesUsersFixture::LOAD,
        InitiativesFixture::LOAD,
        InitiativesTagsFixture::LOAD,
    ];

    private const SME = 2;

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
    }

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/scorecard/';
    }

    public function testGetList_ScorecardSummary()
    {
        InitiativesUsersTable::load()->updateAll(
            ['state' => InitiativesUsersStates::IN_PROGRESS], []);
        $expected = $this->_getExpected();

        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    public function testGetList_ScorecardSummary_FilterDate()
    {
        $start = new FrozenTime('2021-01-01');
        $end = new FrozenTime('2021-01-31');
        $query = '?date:gte=' . $start->jsonSerialize() . '&date:lte=' . $end->jsonSerialize();

        $this->get($this->_getEndpoint() . $query);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEmpty($bodyDecoded['data']);
    }

    public function testGetList_ScorecardSummary_GroupedByState()
    {
        InitiativesUsersTable::load()->updateAll(
            ['state' => InitiativesUsersStates::IN_PROGRESS], ['id' => 1]);
        InitiativesUsersTable::load()->updateAll(
            ['state' => InitiativesUsersStates::COMPLETED], ['id' => 2]);
        $filter = '?group=state';
        $expected = $this->_getExpectedGrouped();

        $this->get($this->_getEndpoint() . $filter);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    public function testGetList_GroupedByStateFilteredByDate_Empty()
    {
        InitiativesUsersTable::load()->updateAll(
            ['state' => InitiativesUsersStates::IN_PROGRESS], []);
        $query = '?group=state';
        $start = new FrozenTime('2021-01-01');
        $end = new FrozenTime('2021-01-31');
        $query .= '&date:gte=' . $start->jsonSerialize() . '&date:lte=' . $end->jsonSerialize();

        $this->get($this->_getEndpoint() . $query);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEmpty($bodyDecoded['data']);
    }

    public function testGetList_GroupedByStateAsSME_SeeOnlyEmployees()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
        UsersTable::load()->updateAll(['access_level' => self::SME],
            ['id' => UsersFixture::SELLER_ID]);
        //Cache::delete('_getFirst' . UsersFixture::SELLER_ID, 'extralong');
        $smeEmployeeId = 4;
        $refectionClass = new ReflectionClass(ScorecardSummaryController::class);
        $reflectionReturn = $refectionClass->getProperty("return");
        $reflectionReturn->setAccessible(true);
        $expected = $this->_getExpectedGrouped()[0];
        unset($expected['initiative']['created']);
        InitiativesUsersTable::load()->updateAll(
            ['state' => InitiativesUsersStates::IN_PROGRESS, 'user_id' => $smeEmployeeId],
            ['id' => 1]);

        $oauthMock = $this->createMock(OAuthServerComponent::class);
        $oauthMock->expects($this->any())
            ->method('getUserIdsBySme')->willReturn([$smeEmployeeId]);
        $oauthMock->expects($this->any())->method('isSmeUser')->willReturn(true);

        $request = new ServerRequest();
        $request = $request->withQueryParams(['group' => 'state']);

        $controller = new ScorecardSummaryController();
        $controller->OAuthServer = $oauthMock;
        $controller->InitiativesUsers = InitiativesUsersTable::load();
        $controller->setRequest($request);

        $controller->getList();

        $res = $reflectionReturn->getValue($controller);
        unset($res[0]['initiative']['created']);
        $this->assertEquals(1, count($res));
        $this->assertEquals($expected, $res[0]);
    }

    private function _getExpected(): array
    {
        return [
            [
                'avg_percentage' => 100,
                'initiative_id' => 1,
                'initiative' => [
                    'id' => 1,
                    'title' => 'SCRUM 101 Course',
                    'description' => 'Description of the course',
                    'created' => '2021-03-01T10:00:00+00:00',
                    'modified' => '2021-03-01T10:00:00+00:00',
                    'tags_list' => ['Agile methodologies']
                ]
            ],
            [
                'avg_percentage' => 50,
                'initiative_id' => 2,
                'initiative' => [
                    'id' => 2,
                    'title' => 'Proyect management master of the universe',
                    'description' => 'Blablabla',
                    'created' => '2021-03-01T10:00:00+00:00',
                    'modified' => '2021-03-01T10:00:00+00:00',
                    'tags_list' => []
                ]
            ]
        ];
    }

    private function _getExpectedGrouped(): array
    {
        return [
            [
                'not_started' => 0,
                'in_progress' => 1,
                'completed' => 0,
                'avg_not_started' => 0,
                'avg_in_progress' => 100,
                'avg_completed' => 0,
                'initiative' => [
                    'id' => 1,
                    'title' => 'SCRUM 101 Course',
                    'created' => '2021-03-01T10:00:00+00:00',
                    'tags_list' => ['Agile methodologies']
                ],
            ],
            [
                'not_started' => 0,
                'in_progress' => 0,
                'completed' => 1,
                'avg_not_started' => 0,
                'avg_in_progress' => 0,
                'avg_completed' => 50,
                'initiative' => [
                    'id' => 2,
                    'title' => 'Proyect management master of the universe',
                    'created' => '2021-03-01T10:00:00+00:00',
                    'tags_list' => []
                ],
            ],
        ];
    }
}

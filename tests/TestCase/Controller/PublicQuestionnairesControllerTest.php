<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Test\Fixture\FieldsFixture;
use Assessment\Test\Fixture\FieldsQuestionnairesFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class PublicQuestionnairesControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        QuestionnairesFixture::LOAD,
        FieldsQuestionnairesFixture::LOAD,
        FieldsFixture::LOAD,
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/public/questionnaires/';
    }

    public function testGetList_GetsAllQuestionnairesWithFields()
    {
        $this->currentAccessToken = null;
        $expectedTotal = 2;
        $expectedQuestionnaire = $this->_getExpectedQuestionnaire();

        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedTotal, count($bodyDecoded['data']));
        $this->assertEquals($expectedQuestionnaire, $bodyDecoded['data'][0]);
    }

    private function _getExpectedQuestionnaire(): array
    {
        $expectedQuestionnaire = [
            'id' => 1,
            'title' => 'Onboarding questions',
            'slug' => 'onboarding',
            'selector_text' => null,
            'type' => null,
            'icon' => null,
            'related_questionnaire_id' => null,
            'language' => Languages::ENG,
            'signup_description' => 'After signup you can blabla',
            'pdf_description_1' => 'this is the pdf one',
            'pdf_description_2' => 'this is the pdf two',
            'pdf_image' => 'www.test.com/test_image.com',
            'level_1' => 'level one text',
            'level_2' => 'level two text',
            'level_3' => 'level three text',
            'level_4' => 'level four text',
            'pdf_title' => null,
            'level_1_title' => null,
            'level_2_title' => null,
            'level_3_title' => null,
            'level_4_title' => null,
            'user_fields_component' => 'bla',
            'hide_user_fields' => true,
            'user_submission_component' => 'blabla',
            'single_submit' => false,
            'checkbox_label' => 'check box label text',
            'email_label' => 'email_label text',
            'question_info' => 'question_info text',
            'user_info' => 'user_info text',
            'fields' => [
                [
                    'id' => 1,
                    'title' => 'Core',
                    'slug' => 'core',
                    'icon' => 'atom',
                    'position' => 1
                ]
            ]
        ];
        return $expectedQuestionnaire;
    }
}

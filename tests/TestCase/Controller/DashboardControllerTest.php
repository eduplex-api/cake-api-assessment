<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use Assessment\AssessmentPlugin;
use Assessment\Controller\DashboardController;
use Assessment\Controller\ScorecardSummaryController;
use App\Controller\Component\OAuthServerComponent;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\Lib\Consts\InitiativesUsersStates;
use Assessment\Model\Table\InitiativesUsersTable;
use Assessment\Test\Fixture\InitiativesFixture;
use Assessment\Test\Fixture\InitiativesTagsFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use ReflectionClass;
use RestApi\TestSuite\ApiCommonErrorsTest;

class DashboardControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        InitiativesUsersFixture::LOAD,
        InitiativesFixture::LOAD,
        InitiativesTagsFixture::LOAD,
    ];


    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
    }

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/dashboard/';
    }

    public function testGetList_withScorecardSummary_OfTopPerformers()
    {
        $start = new FrozenTime('2021-03-01');
        $end = new FrozenTime('2021-03-31');
        $query = '?questionnaire_id=1&date:gte=' . $start->jsonSerialize() . '&date:lte=' . $end->jsonSerialize();
        InitiativesUsersTable::load()->updateAll(
            ['state' => InitiativesUsersStates::IN_PROGRESS], []);
        $expected = $this->_getExpectedTopPerformers();

        $this->get($this->_getEndpoint() . 'top_performers' . $query);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    public function testGetList_ScorecardSummaryFilteredByQuestionnaire()
    {
        $query = '?questionnaire_id=2';

        $this->get($this->_getEndpoint() . 'top_performers' . $query);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEmpty($bodyDecoded['data']);
    }

    public function testGetList_ScorecardSummary_TotalByState()
    {
        $start = new FrozenTime('2021-03-01');
        $end = new FrozenTime('2021-03-31');
        $query = '?date:gte=' . $start->jsonSerialize() . '&date:lte=' . $end->jsonSerialize();
        InitiativesUsersTable::load()->updateAll(
            ['state' => InitiativesUsersStates::IN_PROGRESS], []);
        $expected = $this->_getExpectedTotalByState();

        $this->get($this->_getEndpoint() . 'total_state' . $query);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    public function testGetList_ScorecardSummary_TotalByScore()
    {
        $expected = $this->_getExpectedTotalByScore();

        $this->get($this->_getEndpoint() . 'total_score');

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    public function testGetList_ScorecardSummaryAsSmeNoUsers_Empty()
    {
        $refectionClass = new ReflectionClass(ScorecardSummaryController::class);
        $reflectionReturn = $refectionClass->getProperty("return");
        $reflectionReturn->setAccessible(true);
        $expected = [
            'range-1' => 0,
            'range-2' => 0,
            'range-3' => 0,
            'range-4' => 0,
            'range-5' => 0,
            'range-6' => 0,
            'range-7' => 0
        ];

        $oauthMock = $this->createMock(OAuthServerComponent::class);
        $oauthMock->expects($this->any())
            ->method('getUserIdsBySme')->willReturn([]);
        $oauthMock->expects($this->any())->method('isSmeUser')->willReturn(true);
        $request = new ServerRequest();
        $controller = new DashboardController();
        $controller->OAuthServer = $oauthMock;
        $controller->InitiativesUsers = InitiativesUsersTable::load();
        $controller->setRequest($request);

        $controller->getData('total_score');

        $res = $reflectionReturn->getValue($controller);
        $this->assertEquals($expected, $res);
    }

    public function testGetList_ScorecardSummary_TotalByScoreFilteredByDate()
    {
        $expected = [
            'range-1' => 0,
            'range-2' => 0,
            'range-3' => 0,
            'range-4' => 0,
            'range-5' => 0,
            'range-6' => 0,
            'range-7' => 0
        ];
        $start = new FrozenTime('2021-01-01');
        $end = new FrozenTime('2021-01-31');
        $query = '?date:gte=' . $start->jsonSerialize() . '&date:lte=' . $end->jsonSerialize();

        $this->get($this->_getEndpoint() . 'total_score' . $query);

        $this->assertJsonResponseOK();
        $bodyDecoded = json_decode($this->_getBodyAsString(), true);
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    public function testGetList_ScorecardSummary_TotalByScoreGroupedByInitiative()
    {
        $expected = $this->_getExpectedTotalByScoreGrouped();
        $start = new FrozenTime('2021-03-01');
        $end = new FrozenTime('2021-03-31');
        $query = '?date:gte=' . $start->jsonSerialize() . '&date:lte=' . $end->jsonSerialize();

        $this->get($this->_getEndpoint() . 'score_grouped' . $query);

        $this->assertJsonResponseOK();
        $bodyDecoded = json_decode($this->_getBodyAsString(), true);
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    private function _getExpectedTopPerformers(): array
    {
        return [
            [
                'avg_percentage' => 75,
                'user' => [
                    'id' => 3,
                    'firstname' => 'Regular',
                    'lastname' => 'User',
                    'email' => 'buyer@example.com',
                    'group_id' => 3,
                    'created' => '2021-03-02T10:00:00+00:00',
                    'modified' => '2021-03-02T10:00:00+00:00',
                ]
            ]
        ];
    }

    private function _getExpectedTotalByState(): array
    {
        return [
            [
                'state' => InitiativesUsersStates::IN_PROGRESS,
                'amount' => 2
            ]
        ];
    }

    private function _getExpectedTotalByScore(): array
    {
        return [
            'range-1' => 0,
            'range-2' => 0,
            'range-3' => 1,
            'range-4' => 0,
            'range-5' => 0,
            'range-6' => 0,
            'range-7' => 1,
        ];
    }

    private function _getExpectedTotalByScoreGrouped(): array
    {
        return [
            'Agile methodologies' => [
                'range-1' => 0,
                'range-2' => 0,
                'range-3' => 0,
                'range-4' => 0,
                'range-5' => 1,
            ]
        ];
    }
}

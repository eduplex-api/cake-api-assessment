<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Model\Entity\Question;
use Assessment\Model\Table\QuestionsTable;
use Assessment\Test\Fixture\AnswersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Assessment\Test\Fixture\QuestionsFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class QuestionsControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        QuestionnairesFixture::LOAD,
        QuestionsFixture::LOAD,
        AnswersFixture::LOAD,
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/questionnaires/'
            . QuestionnairesFixture::QUESTIONNAIRE_ID . '/questions/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testGetList_GetsAllQuestionsWithAnswersFilterByQuestionnaireSlug()
    {
        $expectedTotal = 1;
        QuestionsTable::load()->updateAll(['questionnaire_id' => 2], ['id' => 2]);
        $expectedQuestion = [
            'id' => 1,
            'question' => 'Do you follow a project check list to ensure all requirements are met?',
            'tag' => 'Project Financing Analysis',
            'alt1' => 'Alternative one',
            'alt2' => 'Alternative two',
            'position' => 2,
            'questionnaire_id' => 1,
            'answers' => [
                [
                    'id' => 1,
                    'question_id' => 1,
                    'answer' => 'Yes, always',
                    'weight' => 2,
                ],
                [
                    'id' => 2,
                    'question_id' => 1,
                    'answer' => 'Yes, sometimes when it is necessary',
                    'weight' => 1,
                ],
                [
                    'id' => 3,
                    'question_id' => 1,
                    'answer' => 'No, never',
                    'weight' => 0,
                ]
            ]
        ];

        $this->get($this->_getEndpoint() . '?sort_by=position&order=desc');

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedTotal, count($bodyDecoded['data']));
        $this->assertEquals($expectedQuestion, $bodyDecoded['data'][0]);
    }

    public function testAddNew_SellerToken_ForbiddenException()
    {
        $expectedException = 'Resource not allowed with this token';
        $data = [
            'question' => 'example'
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertResponseCode(403);
        $this->assertResponseContains($expectedException);
    }

    public function testAddNew_EmptyQuestion_Exception()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $expectedEmptyException = '"tag":{"_empty":"This field cannot be left empty"}';
        $expectedMissing = '"question":{"_required":"This field is required"}';
        $data = [
            'tag' => '',
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertResponseCode(400);
        $this->assertResponseContains($expectedEmptyException);
        $this->assertResponseContains($expectedMissing);
    }

    public function testAddNew_CreatesNewQuestion()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $data = [
            'question' => 'Do you comunicate with your employees',
            'alt1' => 'Alternative one',
            'alt2' => 'Alternative two',
            'tag' => 'Management',
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];

        $this->assertEquals($data['question'], $response['question']);
        $this->assertEquals($data['tag'], $response['tag']);
        $this->assertEquals($data['questionnaire_id'], $response['questionnaire_id']);
        $this->assertEquals($data['alt1'], $response['alt1']);
        $this->assertEquals($data['alt2'], $response['alt2']);
        /** @var Question $qSaved */
        $qSaved = QuestionsTable::load()->get($response['id']);
        $this->assertEquals(Languages::ENG, $qSaved->language);
    }

    public function testEdit_EditsQuestion()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $questionId = 1;
        $data = [
            'question' => 'Do you communicate with your employees',
            'position' => 99,
            'alt1' => 'Alternative one',
            'alt2' => 'Alternative two',
        ];

        $this->patch($this->_getEndpoint() . $questionId, $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($data['question'], $response['question']);
        $this->assertEquals($data['position'], $response['position']);
        $this->assertEquals($data['alt1'], $response['alt1']);
        $this->assertEquals($data['alt2'], $response['alt2']);
    }

    public function testDelete_DeletesQuestion()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $questionId = 1;

        $this->delete($this->_getEndpoint() . $questionId);

        $this->assertResponseOk($this->_getBodyAsString());
        /** @var Question $qSaved */
        $qSaved = QuestionsTable::load()->findById($questionId)->first();
        $this->assertNull($qSaved);
    }
}

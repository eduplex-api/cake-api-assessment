<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Model\Table\AnswersTable;
use Assessment\Test\Fixture\AnswersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Assessment\Test\Fixture\QuestionsFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class AnswersControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        QuestionsFixture::LOAD,
        AnswersFixture::LOAD,
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/questionnaires/' .
            QuestionnairesFixture::QUESTIONNAIRE_ID . '/questions/' .
            QuestionsFixture::QUESTION_ID . '/answers/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testGetList_GetsAllAnswersFromQuestion()
    {
        $expectedQuestions = [
            [
                'id' => 1,
                'question_id' => 1,
                'answer' => 'Yes, always',
                'weight' => 2,
            ],
            [
                'id' => 2,
                'question_id' => 1,
                'answer' => 'Yes, sometimes when it is necessary',
                'weight' => 1,
            ],
            [
                'id' => 3,
                'question_id' => 1,
                'answer' => 'No, never',
                'weight' => 0,
            ]
        ];

        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expectedQuestions, $bodyDecoded['data']);
    }

    public function testAddNew_SellerToken_ForbiddenException()
    {
        $expectedException = 'Resource not allowed with this token';
        $data = [
            'answer' => 'example'
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertResponseCode(403);
        $this->assertResponseContains($expectedException);
    }

    public function testAddNew_EmptyAnswer_Exception()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $expectedEmptyException = '"answer":{"_empty":"This field cannot be left empty"}';
        $expectedMissing = '"weight":{"_required":"This field is required"}';
        $data = [
            'answer' => '',
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertResponseCode(400);
        $this->assertResponseContains($expectedEmptyException);
        $this->assertResponseContains($expectedMissing);
    }

    public function testAddNew_CreatesNewAnswer()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $expected = [
            'id' => 7,
            'question_id' => QuestionsFixture::QUESTION_ID,
            'answer' => 'Yes',
            'weight' => 0,
        ];
        $data = [
            'answer' => 'Yes',
            'weight' => 0,
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];

        $this->assertEquals($expected, $response);
    }

    public function testEdit_EditsAnswer()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $data = [
            'answer' => 'no',
            'weight' => 10
        ];

        $this->patch($this->_getEndpoint() . AnswersFixture::ANSWER_ID, $data);

        $response = $this->assertJsonResponseOK()['data'];
        $this->assertEquals($data['answer'], $response['answer']);
        $this->assertEquals($data['weight'], $response['weight']);
    }

    public function testDelete_DeletesAnswer()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);

        $this->delete($this->_getEndpoint() . AnswersFixture::ANSWER_ID);

        $this->assertResponseOk($this->_getBodyAsString());
        $answer = AnswersTable::load()->findById(AnswersFixture::ANSWER_ID)->first();
        $this->assertNull($answer);
    }
}

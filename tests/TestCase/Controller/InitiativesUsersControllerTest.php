<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Controller\Component\OAuthServerComponent;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Controller\InitiativesUsersController;
use Assessment\Lib\Consts\InitiativesUsersStates;
use Assessment\Model\Entity\InitiativesUser;
use Assessment\Model\Entity\Submission;
use Assessment\Model\Table\InitiativesUsersTable;
use Assessment\Model\Table\SubmissionsTable;
use Assessment\Test\Fixture\InitiativesFixture;
use Assessment\Test\Fixture\InitiativesTagsFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Assessment\Test\Fixture\SubmissionsFixture;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use Cake\TestSuite\Fixture\FixtureStrategyInterface;
use Cake\TestSuite\Fixture\TruncateStrategy;
use ReflectionClass;
use RestApi\TestSuite\ApiCommonErrorsTest;

class InitiativesUsersControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        InitiativesUsersFixture::LOAD,
        InitiativesFixture::LOAD,
        InitiativesTagsFixture::LOAD,
        QuestionnairesFixture::LOAD,
        SubmissionsFixture::LOAD
    ];

    protected function getFixtureStrategy(): FixtureStrategyInterface
    {
        return new TruncateStrategy();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
    }

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/questionnaires/' .
            QuestionnairesFixture::QUESTIONNAIRE_ID . '/users/' .
            UsersFixture::BUYER_ID . '/initiatives/';
    }

    public function testGetList_GetsAllLastInitiativesUsers()
    {
        $expectedTotal = 1;
        InitiativesUsersTable::load()->updateAll(
            ['questionnaire_id' => 2], ['id' => 2]);
        $expectedInitiativesUsers = [
            'id' => InitiativesUsersFixture::INITIATIVES_USERS_ID,
            'initiative_id' => InitiativesFixture::INITIATIVE_ID,
            'user_id' => UsersFixture::BUYER_ID,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'manager_id' => null,
            'submission_id' => 2,
            'score' => 100,
            'max_score' => 100,
            'percentage' => 100,
            'state' => null,
            'is_flagged' => false,
            'combined_amount' => 1,
            'created' => '2021-03-09T10:00:00+00:00',
            'modified' => '2021-03-01T10:00:00+00:00',
            'initiative' => [
                'id' => 1,
                'title' => 'SCRUM 101 Course',
                'description' => 'Description of the course',
                'created' => '2021-03-01T10:00:00+00:00',
                'modified' => '2021-03-01T10:00:00+00:00',
                'tags_list' => ['Agile methodologies']
            ]
        ];
        $start = new FrozenTime('2021-01-01');
        $end = new FrozenTime('2022-01-01');
        $query = '?questionnaire_id=1&date:gte=' . $start->jsonSerialize() . '&date:lte=' . $end->jsonSerialize();

        $this->get($this->_getEndpoint() . $query);

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedTotal, count($bodyDecoded['data']));
        $this->assertEquals($expectedInitiativesUsers, $bodyDecoded['data'][0]);
    }

    public function testGetList_InitiativesUsersAsEducationManager_InitiativesUsersFiltered()
    {
        $refectionClass = new ReflectionClass(InitiativesUsersController::class);
        $reflectionReturn = $refectionClass->getProperty("return");
        $reflectionReturn->setAccessible(true);
        $expectedInitiatives = 2;
        $oauthMock = $this->createMock(OAuthServerComponent::class);
        $oauthMock->expects($this->any())
            ->method('checkUserBelongsToSme')->willReturn(true);
        $oauthMock->expects($this->any())->method('isSmeUser')->willReturn(false);
        $oauthMock->expects($this->any())->method('isTrainerUser')->willReturn(true);
        $oauthMock->expects($this->any())->method('getTrainerParent')->willReturn(50);
        $request = new ServerRequest(['params' => [
            'userID' => ''.UsersFixture::BUYER_ID,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
        ],]);
        $controller = new InitiativesUsersController();
        $controller->OAuthServer = $oauthMock;
        $controller->InitiativesUsers = InitiativesUsersTable::load();
        $controller->setRequest($request);

        $controller->getList();

        $res = json_decode(json_encode($reflectionReturn->getValue($controller)), true);
        $this->assertEquals($expectedInitiatives, count($res));
    }

    public function testAddNew_CreatesManyInitiativesUsersDeletesOldOnes()
    {
        InitiativesUsersTable::load()->save(new InitiativesUser([
            'initiative_id' => 2,
            'user_id' => UsersFixture::BUYER_ID,
            'questionnaire_id' => 2,
            'manager_id' => null,
            'score' => 50,
            'max_score' => 100,
            'percentage' => 50,
            'is_flagged' => true,
        ]));
        $initiative2Id = 2;
        $expected = [
            [
                'id' => 4,
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
                'manager_id' => null,
                'submission_id' => null,
                'score' => 2,
                'max_score' => 10,
                'percentage' => 20,
                'state' => null,
                'is_flagged' => false,
                'combined_amount' => 1,
                'created' => '2021-03-10T10:00:00+00:00',
                'initiative' => [
                    'id' => 1,
                    'title' => 'SCRUM 101 Course',
                    'description' => 'Description of the course',
                    'created' => '2021-03-01T10:00:00+00:00',
                    'modified' => '2021-03-01T10:00:00+00:00',
                    'tags_list' => ['Agile methodologies']
                ]
            ],
            [
                'id' => 5,
                'initiative_id' => $initiative2Id,
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
                'manager_id' => null,
                'submission_id' => null,
                'score' => 4,
                'max_score' => 5,
                'percentage' => 80,
                'state' => null,
                'is_flagged' => false,
                'combined_amount' => 1,
                'created' => '2021-03-10T10:00:00+00:00',
                'initiative' => [
                    'id' => 2,
                    'title' => 'Proyect management master of the universe',
                    'description' => 'Blablabla',
                    'created' => '2021-03-01T10:00:00+00:00',
                    'modified' => '2021-03-01T10:00:00+00:00',
                    'tags_list' => []
                ]
            ]
        ];
        $data = [
            [
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'score' => 2,
                'max_score' => 10,
                'created' => '2021-03-10T10:00:00+00:00',
            ],
            [
                'initiative_id' => $initiative2Id,
                'score' => 4,
                'max_score' => 5,
                'created' => '2021-03-10T10:00:00+00:00',
            ],
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];
        unset($response[0]['modified']);
        unset($response[1]['modified']);

        $this->assertEquals($expected, $response);
        $userInitiatives = InitiativesUsersTable::load()->find()
            ->where([
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID
            ])->count();
        $this->assertEquals(2, $userInitiatives);
        $initiativeOtherQuestionnaire = InitiativesUsersTable::load()->get(3);
        $this->assertNotNull($initiativeOtherQuestionnaire);
    }

    public function testAddNew_WithUserInfo_CreateSubmission()
    {
        InitiativesUsersTable::load()->save(new InitiativesUser([
            'initiative_id' => 2,
            'user_id' => UsersFixture::BUYER_ID,
            'questionnaire_id' => 2,
            'manager_id' => null,
            'score' => 50,
            'max_score' => 100,
            'percentage' => 50,
            'is_flagged' => true,
        ]));
        $initiative2Id = 2;
        $expectedSubmission = [
            'id' => 3,
            'user_id' => UsersFixture::BUYER_ID,
            'job_position' => 'Manager',
            'location' => 'Vigo',
            'occupation' => 'ESCO position',
            'working_times' => 'Full time',
            'home_office' => true,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'review' => null,
            'rating' => null,
        ];
        $data = [
            'initiatives' => [
                [
                    'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                    'score' => 2,
                    'max_score' => 10,
                    'created' => '2021-03-10T10:00:00+00:00',
                ],
                [
                    'initiative_id' => $initiative2Id,
                    'score' => 4,
                    'max_score' => 5,
                    'created' => '2021-03-10T10:00:00+00:00',
                ]
            ],
            'user' => [
                'job_position' => 'Manager',
                'location' => 'Vigo',
                'occupation' => 'ESCO position',
                'working_times' => 'Full time',
                'home_office' => true,
            ]
        ];

        $this->post($this->_getEndpoint(), $data);

        $response =  $this->assertJsonResponseOK()['data'];

        /** @var Submission $submission */
        $submission = SubmissionsTable::load()->find()->all()->last();
        $this->assertTrue($submission->created->isToday());
        $this->assertEquals($response[0]['submission_id'], $submission->id);
        $this->assertEquals($response[1]['submission_id'], $submission->id);
        $submission = $submission->toArray();
        unset($submission['created']);
        unset($submission['modified']);
        $this->assertEquals($expectedSubmission, $submission);
    }

    public function testAddNew_ManagerRespondsForOtherUser_DeleteOnlyOtherInitiativesUsersWithSameManagerIdUserAndQuestionnaire()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        InitiativesUsersTable::load()->save(new InitiativesUser([
            'initiative_id' => 2,
            'user_id' => UsersFixture::BUYER_ID,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            'manager_id' => UsersFixture::ADMIN_ID,
            'score' => 2,
        ]));
        $initiative2Id = 2;
        $data = [
            [
                'initiative_id' => InitiativesFixture::INITIATIVE_ID,
                'score' => 2,
                'max_score' => 10,
                'created' => '2021-03-10T10:00:00+00:00',
            ],
            [
                'initiative_id' => $initiative2Id,
                'score' => 4,
                'max_score' => 5,
                'created' => '2021-03-10T10:00:00+00:00',
            ],
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];

        $this->assertEquals(UsersFixture::ADMIN_ID, $response[0]['manager_id']);
        $this->assertEquals(2, count($response));
        $userInitiatives = InitiativesUsersTable::load()->find()
            ->where([
                'user_id' => UsersFixture::BUYER_ID,
                'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID
            ])->count();
        $this->assertEquals(4, $userInitiatives);
    }

    public function testEdit_EditsStateInitiativesUsers()
    {
        $expectedState = InitiativesUsersStates::IN_PROGRESS;
        $data = [
            'state' => InitiativesUsersStates::IN_PROGRESS,
        ];

        $this->patch($this->_getEndpoint() .
            InitiativesUsersFixture::INITIATIVES_USERS_ID, $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];

        $this->assertEquals($expectedState, $response['state']);
    }

    public function testEdit_EditsIsFlaggedInitiativesUsers()
    {
        $data = [
            'is_flagged' => true,
        ];

        $this->patch($this->_getEndpoint() .
            InitiativesUsersFixture::INITIATIVES_USERS_ID, $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];

        $this->assertTrue($response['is_flagged']);
    }

    public function testEdit_StateUnset_BadRequest()
    {
        $data = [
            'score' => 10,
        ];

        $this->patch($this->_getEndpoint() .
            InitiativesUsersFixture::INITIATIVES_USERS_ID, $data);

        $this->assertResponseCode(400);
        $this->assertResponseContains('missing edit parameters');
    }

    public function testEdit_SetNullState()
    {
        InitiativesUsersTable::load()
            ->updateAll(['state' => InitiativesUsersStates::COMPLETED],
                ['id' => InitiativesUsersFixture::INITIATIVES_USERS_ID]);
        $data = [
            'state' => null,
        ];

        $this->patch($this->_getEndpoint() .
            InitiativesUsersFixture::INITIATIVES_USERS_ID, $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];

        $this->assertNull($response['state']);
    }

    public function testDelete_DeletesInitiativesUsers()
    {
        $this->delete($this->_getEndpoint() . InitiativesUsersFixture::INITIATIVES_USERS_ID);

        $this->assertResponseOk($this->_getBodyAsString());
        $saved = InitiativesUsersTable::load()
            ->findById(InitiativesUsersFixture::INITIATIVES_USERS_ID)->first();
        $this->assertNull($saved);
    }
}

<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Model\Entity\Initiative;
use Assessment\Model\Entity\InitiativesTag;
use Assessment\Model\Entity\InitiativesUser;
use Assessment\Model\Entity\UsersTagsScore;
use Assessment\Model\Table\InitiativesTable;
use Assessment\Model\Table\InitiativesTagsTable;
use Assessment\Model\Table\InitiativesUsersTable;
use Assessment\Model\Table\UsersTagsScoresTable;
use Assessment\Test\Fixture\InitiativesFixture;
use Assessment\Test\Fixture\InitiativesTagsFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Assessment\Test\Fixture\UsersTagsScoresFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class UserSkillScoresControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD, InitiativesTagsFixture::LOAD, InitiativesFixture::LOAD,
        InitiativesUsersFixture::LOAD, UsersTagsScoresFixture::LOAD
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID
            . '/tag_scores/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testAddNew_GetUserInitiativeUserScoreByTag_BadRequest()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = ['tag' => 'Agile methodologies'];

        $this->post($this->_getEndpoint(), $data);

        $this->assertException('Bad Request', 400, 'esco_uris missing');
    }

    public function testAddNew_ByEscoUri()
    {
        $this->markTestSkipped('Mock data from getTagsByEscoUris to run this test');
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = $this->_setupData();
        $expected = [
            [
                'id' => 1,
                'esco_uri' => 'www.test.com',
                'tag' => 'Agile methodologies',
                'score' => 100,
                'max_score' => 100,
            ],
            [
                'id' => 2,
                'esco_uri' => 'www.test.com',
                'tag' => 'SCRUM',
                'score' => 20,
                'max_score' => 40,
            ],
            [
                'id' => 3,
                'esco_uri' => 'www.test.com',
                'tag' => 'gp',
                'score' => 20,
                'max_score' => 20,
            ]
        ];

        $this->post($this->_getEndpoint(), $data);

        $response = $this->assertJsonResponseOK();
        $this->assertEquals($expected, $response['data']);
    }

    private function _setupData(): array
    {
        InitiativesTable::load()->save(new Initiative([
            'id' => 4,
            'language' => Languages::ENG,
            'title' => 'GP Initiative',
        ]));
        InitiativesTagsTable::load()->save(
            new InitiativesTag(['initiative_id' => 4, 'tag' => 'gp']));
        InitiativesUsersTable::load()->save(new InitiativesUser([
            'initiative_id' => 4,
            'user_id' => UsersFixture::BUYER_ID,
            'score' => 50,
            'max_score' => 100,

        ]));
        UsersTagsScoresTable::load()->save(new UsersTagsScore([
            'user_id' => UsersFixture::BUYER_ID,
            'tag' => 'gp',
            'score' => 20,
            'max_score' => 20,
        ]));
        $data = ['esco_uris' => ['http://data.europa.eu/esco/skill/19a8293b-8e95-4de3-983f-77484079c389']];
        return $data;
    }
}

<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Model\Entity\InitiativesUser;
use Assessment\Model\Table\InitiativesUsersTable;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class UsersQuestionnairesControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        InitiativesUsersFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/usersQuestionnaires/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testGetList_GetsUserIdsAnsweredQuestionnaire()
    {
        $expectedUsers = [UsersFixture::BUYER_ID];
        $this->seedInitiativesUsers();

        $this->get($this->_getEndpoint() . '?questionnaire_answered=1&questionnaire_unanswered=2');

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedUsers, $bodyDecoded['data']);
    }

    public function testGetList_GetsUserIdsAnsweredQuestionnaire_MissingParameters()
    {
        $this->seedInitiativesUsers();

        $this->get($this->_getEndpoint() . '?questionnaire_answered=1');

        $this->assertException('Bad Request', 400, 'Missing parameters');
    }

    public function testGetList_GetsAllQuestionsWithAnswersFilterByQuestionnaireSlug()
    {
        $expectedUsers = [UsersFixture::BUYER_ID];
        $this->seedInitiativesUsers();

        $this->get($this->_getEndpoint() . '?questionnaire_answered=1&questionnaire_unanswered=2');

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedUsers, $bodyDecoded['data']);
    }

    public function seedInitiativesUsers(): void
    {
        $initiativesUsers = [
            new InitiativesUser([
                'user_id' => 4,
                'initiative_id' => 2,
                'score' => 0,
                'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID,
            ]),
            new InitiativesUser([
                'user_id' => 4,
                'initiative_id' => 2,
                'score' => 0,
                'questionnaire_id' => 2,
            ]),
        ];
        InitiativesUsersTable::load()->saveMany($initiativesUsers);
    }

}

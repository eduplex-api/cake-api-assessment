<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Lib\Consts\LanguagesISO6393 as Languages;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Model\Entity\Initiative;
use Assessment\Model\Entity\Question;
use Assessment\Model\Table\InitiativesTable;
use Assessment\Test\Fixture\InitiativesFixture;
use Assessment\Test\Fixture\InitiativesTagsFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class InitiativesControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        InitiativesFixture::LOAD,
        InitiativesTagsFixture::LOAD
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/initiatives/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testGetList_GetsAllInitiatives()
    {
        $expectedTotal = 2;
        $expectedInitiative = [
            'id' => 1,
            'title' => 'SCRUM 101 Course',
            'description' => 'Description of the course',
            'created' => '2021-03-01T10:00:00+00:00',
            'modified' => '2021-03-01T10:00:00+00:00',
            'tags_list' => ['Agile methodologies']
        ];

        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();

        $this->assertEquals($expectedTotal, count($bodyDecoded['data']));
        $this->assertEquals($expectedInitiative, $bodyDecoded['data'][0]);
    }

    public function testAddNew_SellerToken_ForbiddenException()
    {
        $expectedException = 'Resource not allowed with this token';
        $data = [
            'title' => 'example'
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertResponseCode(403);
        $this->assertResponseContains($expectedException);
    }

    public function testAddNew_EmptyInitiative_Exception()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $expectedEmptyException = '"title":{"_empty":"This field cannot be left empty"}';
        $data = [
            'title' => '',
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertResponseCode(400);
        $this->assertResponseContains($expectedEmptyException);
    }

    public function testAddNew_CreatesNewInitiative()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $data = [
            'title' => 'How to manage',
            'description' => 'description example',
            'tags_list' => [
                'Management'
            ],
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];
        $data['id'] = 4;
        unset($response['created']);
        unset($response['modified']);
        $this->assertEquals($data, $response);
        /** @var Initiative $initiativeSaved */
        $initiativeSaved = InitiativesTable::load()->get($data['id']);
        $this->assertEquals(Languages::ENG, $initiativeSaved->language);
    }

    public function testEdit_EditsInitiative()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $data = [
            'title' => 'Do you communicate with your employees',
            'tags_list' => [
                'HR'
            ],
        ];

        $this->patch($this->_getEndpoint() . InitiativesFixture::INITIATIVE_ID, $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];
        $this->assertEquals($data['title'], $response['title']);
        $this->assertEquals($data['tags_list'], $response['tags_list']);
    }

    public function testDelete_DeletesInitiatives()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);

        $this->delete($this->_getEndpoint() . InitiativesFixture::INITIATIVE_ID);

        $this->assertResponseOk($this->_getBodyAsString());
        /** @var Question $qSaved */
        $qSaved = InitiativesTable::load()->findById(InitiativesFixture::INITIATIVE_ID)->first();
        $this->assertNull($qSaved);
    }
}

<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Model\Table\UsersTable;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Test\Fixture\AnswersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Assessment\Test\Fixture\QuestionsFixture;
use Cake\TestSuite\Fixture\FixtureStrategyInterface;
use Cake\TestSuite\Fixture\TruncateStrategy;
use RestApi\TestSuite\ApiCommonErrorsTest;

class UserAnswerControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        QuestionnairesFixture::LOAD,
    ];

    protected function getFixtureStrategy(): FixtureStrategyInterface
    {
        return new TruncateStrategy();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
    }

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/questionnaires/' .
            QuestionnairesFixture::QUESTIONNAIRE_ID . '/users/' .
            UsersFixture::BUYER_ID . '/answers/';
    }

    public function testAddNew_AddUserAnswer()
    {
        $data = [
            'question_id' => QuestionsFixture::QUESTION_ID,
            'question_text'=> 'question test text',
            'answer_id' => AnswersFixture::ANSWER_ID,
            'answer_text' => 'answer test text',
            'start_date' => '2023-02-02 10:00:00'
        ];
        $expected = [
            'question_id' => QuestionsFixture::QUESTION_ID,
            'question_text' => $data['question_text'],
            'answer_id' => AnswersFixture::ANSWER_ID,
            'answer_text' => $data['answer_text'],
            'start_date' => '2023-02-02T10:00:00+00:00',
            'user_id' => UsersFixture::BUYER_ID,
            'manager_id' => null,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];
        $expected['id'] = $response['id'];
        $this->assertEquals($expected, $response);
    }

    public function testAddNew_AddUserAnswerWithoutAnswer()
    {
        $data = [
            'question_id' => QuestionsFixture::QUESTION_ID,
            'question_text'=> 'question test text',
            'answer_id' => null,
            'answer_text' => '',
            'start_date' => '2023-02-02 10:00:00'
        ];
        $expected = [
            'question_id' => QuestionsFixture::QUESTION_ID,
            'question_text' => $data['question_text'],
            'answer_id' => null,
            'answer_text' => '',
            'start_date' => '2023-02-02T10:00:00+00:00',
            'user_id' => UsersFixture::BUYER_ID,
            'manager_id' => null,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];
        $expected['id'] = $response['id'];
        $this->assertEquals($expected, $response);
    }

    public function testAddNew_ManagerRespondsForAnotherUser()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_ADMIN);
        $data = [
            'question_id' => QuestionsFixture::QUESTION_ID,
            'question_text'=> 'question test text',
            'answer_id' => AnswersFixture::ANSWER_ID,
            'answer_text' => 'answer test text',
            'start_date' => '2023-02-02 10:00:00'
        ];
        $expected = [
            'question_id' => QuestionsFixture::QUESTION_ID,
            'question_text' => $data['question_text'],
            'answer_id' => AnswersFixture::ANSWER_ID,
            'answer_text' => $data['answer_text'],
            'start_date' => '2023-02-02T10:00:00+00:00',
            'user_id' => UsersFixture::BUYER_ID,
            'manager_id' => UsersFixture::ADMIN_ID,
            'questionnaire_id' => QuestionnairesFixture::QUESTIONNAIRE_ID
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertJsonResponseOK();
        $response = json_decode($this->_getBodyAsString(), true)['data'];
        $expected['id'] = $response['id'];
        $this->assertEquals($expected, $response);
    }

    public function testAddNew_UserNotManagerCanNotRespondForAnotherUser()
    {
        $notExistingGroup = 66995533;
        UsersTable::load()->updateAll(
            ['group_id' => $notExistingGroup], ['id' => UsersFixture::SELLER_ID]);
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
        $data = [
            'question_id' => QuestionsFixture::QUESTION_ID,
            'question_text'=> 'question test text',
            'answer_id' => AnswersFixture::ANSWER_ID,
            'answer_text' => 'answer test text',
            'start_date' => '2023-02-02 10:00:00'
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertException('Bad Request', 400, 'Cannot respond for another user');
    }
}

<?php
declare(strict_types=1);

namespace Assessment\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use Assessment\AssessmentPlugin;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Assessment\Test\Fixture\SubmissionsFixture;
use RestApi\TestSuite\ApiCommonErrorsTest;

class SubmissionsControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
        OauthAccessTokensFixture::LOAD,
        UsersFixture::LOAD,
        QuestionnairesFixture::LOAD,
        SubmissionsFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return AssessmentPlugin::getRoutePath() . '/users/' . UsersFixture::BUYER_ID . '/submissions/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public function testGetList_GetsAllSubmissionByUserWithQuestionnaires()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $expectedSubmissions = $this->_getExpectedSubmissions();

        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($expectedSubmissions, $bodyDecoded['data']);
    }

    public function testEdit_EditReviewAndRatingInSubmission()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'rating' => 5,
            'review' => 'Amazing questionnaire',
        ];

        $this->patch($this->_getEndpoint() . SubmissionsFixture::SUBMISSION_ID, $data);

        $response = $this->assertJsonResponseOK();
        $this->assertEquals($data['rating'], $response['data']['rating']);
        $this->assertEquals($data['review'], $response['data']['review']);
    }

    private function _getExpectedSubmissions(): array
    {
        $questionnaire = [
            'id' => 1,
            'title' => 'Onboarding questions',
            'slug' => 'onboarding',
            'selector_text' => null,
            'language' => 'en_US',
            'type' => null,
            'icon' => null,
            'related_questionnaire_id' => null,
            'signup_description' => 'After signup you can blabla',
            'pdf_description_1' => 'this is the pdf one',
            'pdf_description_2' => 'this is the pdf two',
            'pdf_image' => 'www.test.com/test_image.com',
            'level_1' => 'level one text',
            'level_2' => 'level two text',
            'level_3' => 'level three text',
            'level_4' => 'level four text',
            'pdf_title' => null,
            'level_1_title' => null,
            'level_2_title' => null,
            'level_3_title' => null,
            'level_4_title' => null,
            'user_fields_component' => 'bla',
            'hide_user_fields' => true,
            'user_submission_component' => 'blabla',
            'single_submit' => false,
            'checkbox_label' => 'check box label text',
            'email_label' => 'email_label text',
            'question_info' => 'question_info text',
            'user_info' => 'user_info text',
            'fields' => []
        ];
        return [
            [
                'id' => 1,
                'user_id' => 3,
                'job_position' => 'Director',
                'occupation' => 'ESCO director',
                'working_times' => 'Full time',
                'home_office' => true,
                'location' => 'Vigo',
                'questionnaire_id' => 1,
                'review' => 'Love it',
                'rating' => 5,
                'created' => '2023-03-02T10:00:00+00:00',
                'modified' => '2023-03-02T10:00:00+00:00',
                'questionnaire' => $questionnaire
            ],
            [
                'id' => 2,
                'user_id' => 3,
                'job_position' => 'Manager',
                'occupation' => 'ESCO Manager',
                'working_times' => 'Part time',
                'home_office' => false,
                'location' => 'Austria',
                'questionnaire_id' => 1,
                'review' => 'Like it',
                'rating' => 4,
                'created' => '2022-03-01T11:00:00+00:00',
                'modified' => '2022-03-01T11:00:00+00:00',
                'questionnaire' => $questionnaire
            ]
        ];
    }


}

<?php
declare(strict_types=1);

namespace Assessment;

use Cake\Routing\RouteBuilder;
use RestApi\Lib\RestPlugin;

class AssessmentPlugin extends RestPlugin
{
    protected function routeConnectors(RouteBuilder $builder): void
    {
        $builder->connect('/questionnaires/{questionnaire_id}/users/{userID}/initiatives/*', \Assessment\Controller\InitiativesUsersController::route());
        $builder->connect('/questionnaires/{questionnaire_id}/users/{userID}/answers/*', \Assessment\Controller\UserAnswerController::route());
        $builder->connect('/questionnaires/{questionnaire_id}/questions/{question_id}/answers/*', \Assessment\Controller\AnswersController::route());
        $builder->connect('/questionnaires/{questionnaire_id}/questions/*', \Assessment\Controller\QuestionsController::route());
        $builder->connect('/usersQuestionnaires/*', \Assessment\Controller\UsersQuestionnairesController::route());
        $builder->connect('/initiatives/*', \Assessment\Controller\InitiativesController::route());
        $builder->connect('/scorecard/*', \Assessment\Controller\ScorecardSummaryController::route());
        $builder->connect('/dashboard/*', \Assessment\Controller\DashboardController::route());
        $builder->connect('/fields/*', \Assessment\Controller\FieldsController::route());
        $builder->connect('/users/{userID}/submissions/*', \Assessment\Controller\SubmissionsController::route());
        $builder->connect('/users/{userID}/job_recommendations/*', \Assessment\Controller\JobRecommendationsController::route());
        $builder->connect('/users/{userID}/tag_scores/*', \Assessment\Controller\UserSkillScoresController::route());
        $builder->connect('/users/{userID}/users_tags_scores/*', \Assessment\Controller\UsersTagsScoresController::route());
        $builder->connect('/questionnaires/*', \Assessment\Controller\QuestionnairesController::route());
        $builder->connect('/public/questionnaires/*', \Assessment\Controller\PublicQuestionnairesController::route());
        $builder->connect('/assessment/openapi/*', \Assessment\Controller\SwaggerJsonController::route());
    }
}

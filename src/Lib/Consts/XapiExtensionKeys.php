<?php
declare(strict_types=1);

namespace Assessment\Lib\Consts;

class XapiExtensionKeys
{
    const COURSE_DATA = 'https://proto.eduplex.eu/xapi/extension/course_data';
    const DEVICE_TOKEN = 'https://proto.eduplex.eu/xapi/extension/device_token';
    const USER_ANSWERS = 'https://proto.eduplex.eu/xapi/extension/user_answers';
}

<?php
declare(strict_types=1);

namespace Assessment\Lib\Consts;

class QuestionnaireTypes
{
    const RADIO = 'radio';
    const SLIDER = 'slider';
}

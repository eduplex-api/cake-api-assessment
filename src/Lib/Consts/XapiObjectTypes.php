<?php
declare(strict_types=1);

namespace Assessment\Lib\Consts;

class XapiObjectTypes
{
    const USER_ANSWERS = 'https://proto.eduplex.eu/xapi/object/types/user_answers';
}

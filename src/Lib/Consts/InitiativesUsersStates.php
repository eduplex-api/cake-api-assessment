<?php

namespace Assessment\Lib\Consts;

class InitiativesUsersStates
{
    const NOT_STARTED = 'not_started';
    const IN_PROGRESS = 'in_progress';
    const COMPLETED = 'completed';
}

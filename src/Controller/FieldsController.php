<?php

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Table\FieldsTable;

/**
 * @property FieldsTable $Fields
 */
class FieldsController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Fields = FieldsTable::load();
    }

    public function getList()
    {
        $this->return = $this->Fields->findAllFields()->all();
    }

    public function getData($id)
    {
        $this->return = $this->Fields->findById($id)->firstOrFail();
    }
}

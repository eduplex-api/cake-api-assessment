<?php
declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Entity\UsersTagsScore;
use Assessment\Model\Table\UsersTagsScoresTable;
use Cake\Http\Exception\BadRequestException;

/**
 * @property \OpenSearch\Model\Table\TagsTable $Tags
 */
class UsersTagsScoresController extends ApiController
{
    public UsersTagsScoresTable $UsersTagsScores;

    public function initialize(): void
    {
        parent::initialize();
        $this->UsersTagsScores = UsersTagsScoresTable::load();
    }

    public function addNew($data)
    {
        $userId = $this->request->getParam('userID');

        if (!isset($data['tag'])) {
            throw new BadRequestException('Tag is mandatory');
        }
        $newScore = $this->UsersTagsScores->newEmptyEntity();
        /** @var UsersTagsScore $newScore */
        $newScore = $this->UsersTagsScores->patchEntity($newScore, $data);
        $newScore->user_id = (int) $userId;
        $newScore = $this->UsersTagsScores->save($newScore);

        /** @var UsersTagsScore $newScore */
        $this->UsersTagsScores->softDeleteOldUserScores($newScore);
        $this->return = $this->UsersTagsScores->findByUserAndTag($userId, $newScore->tag)->first();
    }
}

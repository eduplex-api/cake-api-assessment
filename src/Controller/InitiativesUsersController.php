<?php
declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Entity\InitiativesUser;
use Assessment\Model\Entity\Submission;
use Assessment\Model\Table\InitiativesUsersTable;
use Assessment\Model\Table\SubmissionsTable;
use Cake\Http\Exception\BadRequestException;
use Cake\I18n\FrozenTime;
use RestApi\Controller\Traits\RestApiPaginationTrait;

/**
 * @property InitiativesUsersTable $InitiativesUsers
 * @property SubmissionsTable $Submissions
 */
class InitiativesUsersController extends ApiController
{
    use RestApiPaginationTrait;
    public function initialize(): void
    {
        parent::initialize();
        $this->InitiativesUsers = InitiativesUsersTable::load();
        $this->Submissions = SubmissionsTable::load();
    }

    public function authorizeUserData()
    {
        if (!$this->OAuthServer->isSmeUser()
            && !$this->OAuthServer->isManagerUser()
            && !$this->OAuthServer->isTrainerUser()) {
            throw new BadRequestException('Cannot respond for another user');
        }
        $token = $this->request->getHeaders()['Authorization'][0] ?? '';
        $userId = $this->getUid();
        if ($this->OAuthServer->isSmeUser()) {
            $smeId = $this->OAuthServer->getUserID();
            $this->OAuthServer->checkUserBelongsToSme($token, $smeId, $userId);
        }
        if ($this->OAuthServer->isTrainerUser()) {
            $parentId = $this->OAuthServer->getTrainerParent();
            $this->OAuthServer->checkUserBelongsToSme($token, $parentId, $userId);
        }
    }

    private function getUid(): string
    {
        return $this->request->getParam('userID');
    }

    public function getList()
    {
        $userId = $this->getUid();
        $questionnaireId = $this->request->getParam('questionnaire_id');
        $filters = $this->processQueryFilters($this->request->getQueryParams());
        $this->return = $this->InitiativesUsers
            ->getLastInitiativesUsersByUser($userId, $questionnaireId, $filters);
    }

    public function addNew($data)
    {
        $userId = $this->getUid();
        $questionnaireId = $this->request->getParam('questionnaire_id');
        if ($userId != $this->OAuthServer->getUserID()) {
            $managerId = $this->OAuthServer->getUserID();
        }
        $filters =[];
        $toSave = [];

        if (isset($data['initiatives']) && isset($data['user'])) {
            $arrayInitiativesUser = $data['initiatives'] ;
            /** @var Submission $submission */
            $submission = $this->Submissions->patchEntity(
                $this->Submissions->newEmptyEntity(), $data['user']);
            $submission->user_id = $userId;
            $submission->questionnaire_id = $questionnaireId;
            $submission = $this->Submissions->save($submission);
        } else {
            $arrayInitiativesUser = $data;
        }
        if (!$arrayInitiativesUser[0]['created']) {
            throw new BadRequestException('missing created in user initiative');
        }
        $createdDate = new FrozenTime($arrayInitiativesUser[0]['created']);
        foreach ($arrayInitiativesUser as $uInitiativeData) {
            $initiativeUser = $this->InitiativesUsers->newEmptyEntity();
            /** @var InitiativesUser $initiativeUser */
            $initiativeUser = $this->InitiativesUsers->patchEntity($initiativeUser, $uInitiativeData);
            $initiativeUser->user_id = $userId;
            $initiativeUser->manager_id = $managerId ?? null;
            $initiativeUser->questionnaire_id = $questionnaireId;
            $initiativeUser->created = $createdDate;
            if (isset($submission)) {
                $initiativeUser->submission_id = $submission->id;
            }
            if (isset($initiativeUser->score) && isset($initiativeUser->max_score)
                && $initiativeUser->max_score > 0) {
                $initiativeUser->percentage = round(
                    $initiativeUser->score / $initiativeUser->max_score * 100);
            }
            $toSave[] = $initiativeUser;
        }
        $this->InitiativesUsers->saveMany($toSave);
        $deleteConditions = [
            'questionnaire_id' => $questionnaireId,
            'user_id' => $userId,
            'created !=' => $createdDate
        ];
        if (isset($managerId)) {
            $filters = ['manager_id' => $managerId];
            $deleteConditions['manager_id'] = $managerId;
        }
        $this->InitiativesUsers->softDeleteAll($deleteConditions);
        $this->return = $this->InitiativesUsers
            ->findLastInitiativesUsersByUser($userId, $questionnaireId, $filters)->all();
    }

    public function edit($id, $data)
    {
        $userId = $this->getUid();
        $initiativeUser = $this->InitiativesUsers
            ->findById($id)->where(['user_id' => $userId])->firstOrFail();
        $toPatch = [];
        if (array_key_exists('state', $data)) {
            $toPatch['state'] = $data['state'];
        }
        if (isset($data['is_flagged'])) {
            $toPatch['is_flagged'] = $data['is_flagged'];
        }
        if (empty($toPatch)) {
            throw new BadRequestException('missing edit parameters');
        }
        /** @var InitiativesUser $initiativeUser */
        $this->InitiativesUsers->patchEntity($initiativeUser, $toPatch);
        $saved = $this->InitiativesUsers->saveOrFail($initiativeUser);
        $this->return = $this->InitiativesUsers->findByIdWithInitiatives($saved->id)->firstOrFail();
    }

    public function delete($id)
    {
        $this->InitiativesUsers->softDelete($id);
        $this->return = false;
    }
}

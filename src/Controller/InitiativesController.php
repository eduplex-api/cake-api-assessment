<?php

namespace Assessment\Controller;

use App\Controller\ApiController;
use App\Lib\I18n\LegacyI18n;
use Assessment\Model\Entity\Initiative;
use Assessment\Model\Entity\InitiativesTag;
use Assessment\Model\Table\InitiativesTable;
use Cake\Http\Exception\ForbiddenException;

/**
 * @property InitiativesTable $Initiatives
 */
class InitiativesController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Initiatives = InitiativesTable::load();
    }

    public function main($id = null, $secondParam = null)
    {
        if (!$this->request->is('GET') &&
            !$this->OAuthServer->isManagerUser()) {
            throw new ForbiddenException('Resource not allowed with this token');
        }
        parent::main($id, $secondParam);
    }

    public function getList()
    {
        $this->return = $this->Initiatives->findAllInitiativesWithTags()->all();
    }

    public function getData($id)
    {
        $this->return = $this->Initiatives->findByIdWithTags($id)->firstOrFail();
    }

    public function addNew($data)
    {
        $initiative = $this->Initiatives->newEmptyEntity();
        $initiative = $this->Initiatives->patchEntity($initiative, $data);
        if (isset($data['tags_list'])) {
            $initiativesTags = [];
            foreach ($data['tags_list'] as $tag) {
                $initiativesTags[] = new InitiativesTag([
                    'tag' => $tag,
                ]);
            }
            /** @var Initiative $initiative */
            $initiative->setInitiativesTags($initiativesTags);
            $initiative->language = LegacyI18n::getLocale();
        }
        $saved = $this->Initiatives->saveOrFail($initiative);
        $this->return = $this->Initiatives->findByIdWithTags($saved->id)->firstOrFail();
    }

    public function edit($id, $data)
    {
        $initiative = $this->Initiatives->findByIdWithTags($id)->firstOrFail();
        /** @var Initiative $initiative */
        $initiative = $this->Initiatives->patchEntity($initiative, $data);
        $saved = $this->Initiatives->saveOrFail($initiative);
        if (isset($data['tags_list'])) {
            $this->Initiatives->manageInitiativeTags($initiative, $data['tags_list']);
        }
        $this->return = $this->Initiatives->findByIdWithTags($saved->id)->firstOrFail();
    }

    public function delete($id)
    {
        $this->Initiatives->softDelete($id);
        $this->return = false;
    }
}

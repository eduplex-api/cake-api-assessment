<?php
declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use App\Lib\I18n\LegacyI18n;
use Assessment\Model\Entity\FieldsQuestionnaire;
use Assessment\Model\Entity\Questionnaire;
use Assessment\Model\Table\QuestionnairesTable;
use Cake\Http\Exception\ForbiddenException;

/**
 * @property QuestionnairesTable $Questionnaires
 */
class QuestionnairesController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Questionnaires = QuestionnairesTable::load();
    }

    public function main($id = null, $secondParam = null)
    {
        if (!$this->request->is('GET') &&
            !$this->OAuthServer->isManagerUser()) {
            throw new ForbiddenException('Resource not allowed with this token');
        }
        parent::main($id, $secondParam);
    }

    public function getList()
    {
        $filters = $this->request->getQueryParams();
        if (isset($filters['only_answered'])) {
            $questionnaires = $this->Questionnaires
                ->findAnsweredByUser($this->OAuthServer->getUserID())->all();
        } else {
            $questionnaires = $this->Questionnaires->findAllWithFields()->all();
        }
        $this->return = $questionnaires;
    }

    public function getData($id)
    {
        if (is_numeric($id)) {
            $questionnaire = $this->Questionnaires->findByIdWithFields($id)->firstOrFail();
        } else {
            $questionnaire = $this->Questionnaires->findBySlugWithFields($id)->firstOrFail();
        }
        $this->return = $questionnaire;
    }

    public function addNew($data)
    {
        $questionnaire = $this->Questionnaires->newEmptyEntity();
        /** @var Questionnaire $questionnaire */
        $questionnaire = $this->Questionnaires->patchEntity($questionnaire, $data);
        $questionnaire->language = LegacyI18n::getLocale();
        if (isset($data['fields_ids'])) {
            $fieldsQuestionnaires = [];
            foreach ($data['fields_ids'] as $field_id) {
                $fieldsQuestionnaires[] = new FieldsQuestionnaire([
                    'field_id' => $field_id,
                ]);
            }
            $questionnaire->setFieldsQuestionnaire($fieldsQuestionnaires);
        }
        $saved = $this->Questionnaires->saveOrFail($questionnaire);
        $this->return = $this->Questionnaires->findBySlugWithFields($saved->slug)->firstOrFail();
    }

    public function edit($id, $data)
    {
        if (is_numeric($id)) {
            $questionnaire = $this->Questionnaires->findByIdWithFields($id)->firstOrFail();
        } else {
            $questionnaire = $this->Questionnaires->findBySlugWithFields($id)->firstOrFail();
        }
        /** @var Questionnaire $questionnaire */
        $questionnaire = $this->Questionnaires->patchEntity($questionnaire, $data);
        $saved = $this->Questionnaires->saveOrFail($questionnaire);
        if (isset($data['fields_ids'])) {
            $this->Questionnaires->manageFieldsQuestionnaire($questionnaire, $data['fields_ids']);
        }
        $this->return = $this->Questionnaires->findByIdWithFields($saved->id)->firstOrFail();
    }

    public function delete($id)
    {
        $this->Questionnaires->softDelete($id);
        $this->return = false;
    }
}

<?php
declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Entity\JobRecommendation;
use Assessment\Model\Table\JobRecommendationsTable;

/**
 * @property JobRecommendationsTable $JobRecommendations
 */
class JobRecommendationsController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->JobRecommendations = JobRecommendationsTable::load();
    }

    public function getList()
    {
        $userId = $this->request->getParam('userID');
        $this->return = $this->JobRecommendations->findRecommendationsByUser($userId)->all();
    }

    public function getData($id)
    {
        $userId = $this->request->getParam('userID');
        $this->return = $this->JobRecommendations
            ->findRecommendationsByUser($userId)
            ->where(['id' => $id])
            ->firstOrFail();
    }

    public function addNew($data)
    {
        $userId = $this->request->getParam('userID');
        $recommendation = $this->JobRecommendations->newEmptyEntity();
        /** @var JobRecommendation $recommendation */
        $recommendation = $this->JobRecommendations->patchEntity($recommendation, $data);
        $recommendation->user_id = $userId;
        $this->return = $this->JobRecommendations->save($recommendation);
    }

    public function edit($id, $data)
    {
        $userId = $this->request->getParam('userID');
        /** @var JobRecommendation $recommendation */
        $recommendation = $this->JobRecommendations->findRecommendationsByUser($userId)
            ->where(['JobRecommendations.id' => $id])
            ->firstOrFail();
        $recommendation = $this->JobRecommendations->patchEntity($recommendation, $data);
        $this->return = $this->JobRecommendations->save($recommendation);
    }

    public function delete($id)
    {
        $userId = $this->request->getParam('userID');
        $recommendation = $this->JobRecommendations
            ->findRecommendationsByUser($userId)
            ->where(['id' => $id])
            ->firstOrFail();
        $this->JobRecommendations->softDelete($recommendation->id);
        $this->return = false;
    }
}

<?php
declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Table\InitiativesUsersTable;
use Cake\Http\Exception\BadRequestException;
use RestApi\Controller\Traits\RestApiPaginationTrait;

/**
 * @property InitiativesUsersTable $InitiativesUsers
 */
class UsersQuestionnairesController extends ApiController
{
    use RestApiPaginationTrait;

    public function initialize(): void
    {
        parent::initialize();
        $this->InitiativesUsers = InitiativesUsersTable::load();
    }

    public function getList()
    {
        $qAnswered = $this->request->getQueryParams()['questionnaire_answered'] ?? null;
        $qUnanswered = $this->request->getQueryParams()['questionnaire_unanswered'] ?? null;
        if (!isset($qAnswered) || !isset($qUnanswered)) {
            throw new BadRequestException('Missing parameters');
        }
        $this->return = $this->InitiativesUsers
            ->getUserIdsByAnsweredQuestionnaires($qAnswered, $qUnanswered);
    }
}

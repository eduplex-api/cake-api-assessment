<?php
declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Table\QuestionnairesTable;

/**
 * @property QuestionnairesTable $Questionnaires
 */
class PublicQuestionnairesController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Questionnaires = QuestionnairesTable::load();
    }

    public function isPublicController(): bool
    {
        return true;
    }

    public function getList()
    {
        $questionnaires = $this->Questionnaires->findAllWithFields()->all();
        $this->return = $questionnaires;
    }
}

<?php

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Entity\Answer;
use Assessment\Model\Table\AnswersTable;
use Cake\Http\Exception\ForbiddenException;

/**
 * @property AnswersTable $Answers
 */
class AnswersController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Answers = AnswersTable::load();
    }

    public function main($id = null, $secondParam = null)
    {
        if (!$this->request->is('GET') &&
            !$this->OAuthServer->isManagerUser()) {
            throw new ForbiddenException('Resource not allowed with this token');
        }
        parent::main($id, $secondParam);
    }

    public function getList()
    {
        $questionId = $this->request->getParam('question_id');
        $this->return = $this->Answers->find()
            ->where(['question_id' => $questionId])
            ->all();
    }

    public function addNew($data)
    {
        $questionId = $this->request->getParam('question_id');
        $question = $this->Answers->Questions->get($questionId);
        $answer = $this->Answers->newEmptyEntity();
        $answer = $this->Answers->patchEntity($answer, $data);
        /** @var Answer $answer */
        $answer->question_id = $question->id;
        $saved = $this->Answers->saveOrFail($answer);
        $this->return  = $this->Answers->get($saved->id);
    }

    public function edit($id, $data)
    {
        $answer = $this->Answers->get($id);
        $answer = $this->Answers->patchEntity($answer, $data);
        $saved = $this->Answers->saveOrFail($answer);
        $this->return  = $this->Answers->get($saved->id);
    }

    public function delete($id)
    {
        $this->Answers->softDelete($id);
        $this->return  = false;
    }
}

<?php
declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Table\InitiativesUsersTable;
use Assessment\Model\Table\UsersTagsScoresTable;
use Cake\Http\Exception\BadRequestException;
use OpenSearch\Model\Table\TagsTable;

/**
 * @property TagsTable $Tags
 */
class UserSkillScoresController extends ApiController
{
    public InitiativesUsersTable $InitiativesUser;
    public UsersTagsScoresTable $UsersTagsScores;

    public function initialize(): void
    {
        parent::initialize();
        $this->InitiativesUser = InitiativesUsersTable::load();
        $this->UsersTagsScores = UsersTagsScoresTable::load();
        $this->Tags = TagsTable::load();
    }

    public function addNew($data)
    {
        $userId = $this->request->getParam('userID');
        $toRet = [];
        if (!isset($data['esco_uris'])) {
            throw new BadRequestException('esco_uris missing');
        }
        $tags = $this->Tags->getTagsByEscoUris($data['esco_uris']);
        $tagSlugs = [];
        foreach ($tags as $tag) {
            $tagSlugs[] = $tag['tag'];
            $toRet[] = [
                'id' => $tag['id'],
                'tag' => $tag['tag'],
                'esco_uri' => $tag['esco_uri'],
                'score' => null,
            ];
        }
        $initiativeScores = $this->InitiativesUser->getUserScoreInTags($userId, $tagSlugs);
        $userScores = $this->UsersTagsScores->getUserScoreInTags($userId, $tagSlugs);
        foreach ($toRet as &$tagToRet) {
            $initiativeScore = $initiativeScores[$tagToRet['tag']] ?? null;
            $userTagScore = $userScores[$tagToRet['tag']] ?? null;
            $scoreToRet = $this->_getScoreToReturn($initiativeScore, $userTagScore);
            $tagToRet['score'] = $scoreToRet['score'] ?? null;
            $tagToRet['max_score'] = $scoreToRet['max_score'] ?? null;
        }
        $this->return = $toRet;
    }

    private function _getScoreToReturn($initiativeScore, $userTagScore): array
    {
        $scoreToUse = $initiativeScore ?? [];
        if (!empty($initiativeScore)
            && $initiativeScore['score'] > 0 && $initiativeScore['max_score'] > 0) {
            $initiativePercentage = $initiativeScore['score'] / $initiativeScore['max_score'];
        } else {
            $initiativePercentage = 0;
        }
        if ($userTagScore && $userTagScore['score'] > 0 && $userTagScore['max_score'] > 0) {
            $userTagScorePercentage =  $userTagScore['score'] / $userTagScore['max_score'];
            if ($userTagScorePercentage > $initiativePercentage) {
                $scoreToUse = $userTagScore;
            }
        }
        return $scoreToUse;
    }
}

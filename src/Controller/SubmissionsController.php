<?php
declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Entity\Submission;
use Assessment\Model\Table\SubmissionsTable;
use Cake\Http\Exception\BadRequestException;

/**
 * @property SubmissionsTable $Submissions
 */
class SubmissionsController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Submissions = SubmissionsTable::load();
    }

    public function authorizeUserData()
    {
        if (!$this->OAuthServer->isSmeUser()
            && !$this->OAuthServer->isManagerUser()
            && !$this->OAuthServer->isTrainerUser()) {
            throw new BadRequestException('Cannot get other user submissions');
        }
        $token = $this->request->getHeaders()['Authorization'][0] ?? '';
        $userId = $this->request->getParam('userID');
        if ($this->OAuthServer->isSmeUser()) {
            $smeId = $this->OAuthServer->getUserID();
            $this->OAuthServer->checkUserBelongsToSme($token, $smeId, $userId);
        }
        if ($this->OAuthServer->isTrainerUser()) {
            $parentId = $this->OAuthServer->getTrainerParent();
            $this->OAuthServer->checkUserBelongsToSme($token, $parentId, $userId);
        }
    }

    public function getList()
    {
        $userId = $this->request->getParam('userID');
        $this->return = $this->Submissions->findSubmissionsByUser($userId)->all();
    }

    public function edit($id, $data)
    {
        $userId = $this->request->getParam('userID');
        /** @var Submission $submission */
        $submission = $this->Submissions->findSubmissionsByUser($userId)
            ->where(['Submissions.id' => $id])
            ->firstOrFail();
        $submission = $this->Submissions->patchEntity($submission, $data);
        $this->return = $this->Submissions->save($submission);
    }
}

<?php

declare(strict_types=1);

namespace Assessment\Controller;

use App\Controller\ApiController;
use App\Controller\XAPIProxyBaseController;
use App\Lib\FullBaseUrl;
use Assessment\Lib\Consts\XapiExtensionKeys;
use Assessment\Lib\Consts\XapiObjectTypes;
use Assessment\Model\Entity\UserAnswer;
use Assessment\Model\Table\UserAnswersTable;
use Cake\Http\Client;
use Cake\Http\Exception\BadRequestException;
use Cake\I18n\FrozenTime;

/**
 * @property Client $httpClient
 * @property UserAnswersTable $UserAnswersTable
 */
class UserAnswerController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->httpClient = new Client();
        $this->UserAnswersTable = UserAnswersTable::load();
    }

    public function authorizeUserData()
    {
        if (!$this->OAuthServer->isSmeUser()
            && !$this->OAuthServer->isManagerUser()
            && !$this->OAuthServer->isTrainerUser()) {
            throw new BadRequestException('Cannot respond for another user');
        }
        $token = $this->request->getHeaders()['Authorization'][0] ?? '';
        $userId = $this->request->getParam('userID');
        if ($this->OAuthServer->isSmeUser()) {
            $smeId = $this->OAuthServer->getUserID();
            $this->OAuthServer->checkUserBelongsToSme($token, $smeId, $userId);
        }
        if ($this->OAuthServer->isTrainerUser()) {
            $parentId = $this->OAuthServer->getTrainerParent();
            $this->OAuthServer->checkUserBelongsToSme($token, $parentId, $userId);
        }
    }

    public function addNew($data)
    {
        $userId = $this->request->getParam('userID');
        $questionnaireId = $this->request->getParam('questionnaire_id');
        if ($userId != $this->OAuthServer->getUserID()) {
            $managerId = $this->OAuthServer->getUserID();
        }
        $userAnswer = $this->UserAnswersTable->newEmptyEntity();
        /** @var UserAnswer $userAnswer */
        $userAnswer = $this->UserAnswersTable->patchEntity($userAnswer, $data);
        $userAnswer->user_id = $userId;
        $userAnswer->manager_id = $managerId ?? null;
        $userAnswer->answer_id = $data['answer_id'] ?? null;
        $userAnswer->questionnaire_id = $questionnaireId;
        $saved = $this->UserAnswersTable->saveOrFail($userAnswer);
        $toRet = $this->UserAnswersTable->get($saved->id);
        $this->_saveInLrs($toRet);
        $this->return  = $toRet;
    }

    private function _saveInLrs($data): void
    {
        $envLrsUrl = env('LRS_URL');
        if (!$envLrsUrl) {
            return;
        }
        $url = $envLrsUrl . '/statements';
        $body = json_encode($this->_getXapiProxyStatementBody($data));
        $response = $this->httpClient->post($url, $body, $this->_getXapiProxyRequestOptions());
    }

    private function _getXapiProxyRequestOptions (): array
    {
        return [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => XAPIProxyBaseController::getAuthLrs(),
                'X-Experience-API-Version' => '1.0.1'
            ]
        ];
    }

    private function _getXapiProxyStatementBody ($data): array
    {
        $baseUrl = FullBaseUrl::host();
        $objectId = $baseUrl . "/questionnaire/" . $data['questionnaire_id']
            . "/user/". $data['user_id']
            . "/question/" . $data['question_id']
            . "/answer/" . $data['answer_id'];
        return [
            "actor" => [
                "account" => [
                    "homePage" => $baseUrl,
                    "name" => (string)($data['manager_id'] ?? $data['user_id'])
                ],
                "objectType" => "Agent"
            ],
            "context" => [
                "extensions" => [
                    XapiExtensionKeys::USER_ANSWERS => [
                        "user_id" => $data['user_id'],
                        "manager_id" => $data['manager_id'],
                        "questionnaire_id" => $data['questionnaire_id'],
                        "question_id" => $data['question_id'],
                        "question_text" => $data['question_text'],
                        "answer_id" => $data['answer_id'],
                        "answer_text" => $data['answer_text'],
                        "start_date" => (new FrozenTime($data['start_date']))->toDateTimeString()
                    ]
                ]
            ],
            "object" => [
                "definition" => [
                    "type" => XapiObjectTypes::USER_ANSWERS
                ],
                "id" => $objectId
            ],
            "verb" => [
                "display" => [
                    "en-US" => "answered"
                ],
                "id" => "http://adlnet.gov/expapi/verbs/answered"
            ]
        ];
    }
}

<?php

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Table\InitiativesUsersTable;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use RestApi\Controller\Traits\RestApiPaginationTrait;

/**
 * @property InitiativesUsersTable $InitiativesUsers
 */
class DashboardController extends ApiController
{
    use RestApiPaginationTrait;
    public const TOP_PERFORMERS = 'top_performers';
    public const TOTAL_STATE = 'total_state';
    public const TOTAL_SCORE = 'total_score';
    public const SCORE_GROUPED = 'score_grouped';

    public function initialize(): void
    {
        parent::initialize();
        $this->InitiativesUsers = InitiativesUsersTable::load();
    }

    public function main($id = null, $secondParam = null)
    {
        if (!$this->OAuthServer->isManagerUser() && !$this->OAuthServer->isSmeUser()) {
            throw new ForbiddenException('Resource not allowed with this token');
        }
        parent::main($id, $secondParam);
    }

    public function getData($id)
    {
        $filters = $this->processQueryFilters($this->request->getQueryParams());
        if ($this->OAuthServer->isSmeUser()) {
            $token = $this->request->getHeaders()['Authorization'][0] ?? '';
            $smeId = $this->OAuthServer->getUserID();
            $filters['user_ids'] = $this->OAuthServer->getUserIdsBySme($token, $smeId);
        }
        switch ($id) {
            case self::TOP_PERFORMERS:
                $this->return = $this->InitiativesUsers->getDashboardTopPerformers($filters);
                break;
            case self::TOTAL_STATE:
                $this->return = $this->InitiativesUsers->getDashboardTotalRespondentsByState($filters);
                break;
            case self::TOTAL_SCORE:
                $this->return = $this->InitiativesUsers->getDashboardTotalRespondentsByScore($filters);
                break;
            case self::SCORE_GROUPED:
                $this->return = $this->InitiativesUsers
                    ->getDashboardTotalRespondentsByScoreGroupedByInitiative($filters);
                break;
            default:
                throw new BadRequestException("Invalid chart name: $id");
        }
    }
}

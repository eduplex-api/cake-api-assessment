<?php

namespace Assessment\Controller;

use App\Controller\ApiController;
use App\Lib\I18n\LegacyI18n;
use Assessment\Model\Entity\Question;
use Assessment\Model\Table\QuestionsTable;
use Cake\Http\Exception\ForbiddenException;
use RestApi\Controller\Traits\RestApiPaginationTrait;

/**
 * @property QuestionsTable $Questions
 */
class QuestionsController extends ApiController
{
    use RestApiPaginationTrait;
    public function initialize(): void
    {
        parent::initialize();
        $this->Questions = QuestionsTable::load();
    }

    public function main($id = null, $secondParam = null)
    {
        if (!$this->request->is('GET') &&
            !$this->OAuthServer->isManagerUser()) {
            throw new ForbiddenException('Resource not allowed with this token');
        }
        parent::main($id, $secondParam);
    }

    public function getList()
    {
        $questionnaireId = $this->request->getParam('questionnaire_id');
        $filters = $this->processQueryFilters($this->request->getQueryParams());
        $this->return = $this->Questions
            ->findQuestionsByQuestionnaireWithAnswers($questionnaireId, $filters)->all();
    }

    public function addNew($data)
    {
        $questionnaireId = $this->request->getParam('questionnaire_id');
        $question = $this->Questions->newEmptyEntity();
        $question = $this->Questions->patchEntity($question, $data);
        /** @var Question $question */
        $question->language = LegacyI18n::getLocale();
        $question->questionnaire_id = $questionnaireId;
        $saved = $this->Questions->saveOrFail($question);
        $this->return  = $this->Questions->get($saved->id);
    }

    public function edit($id, $data)
    {
        $question = $this->Questions->get($id);
        $question = $this->Questions->patchEntity($question, $data);
        /** @var Question $question */
        $saved = $this->Questions->saveOrFail($question);
        $this->return  = $this->Questions->get($saved->id);
    }

    public function delete($id)
    {
        $this->Questions->softDelete($id);
        $this->return  = false;
    }
}

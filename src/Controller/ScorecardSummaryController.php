<?php

namespace Assessment\Controller;

use App\Controller\ApiController;
use Assessment\Model\Table\InitiativesUsersTable;
use Cake\Http\Exception\ForbiddenException;
use RestApi\Controller\Traits\RestApiPaginationTrait;

/**
 * @property InitiativesUsersTable $InitiativesUsers
 */
class ScorecardSummaryController extends ApiController
{
    use RestApiPaginationTrait;
    public const GROUP_STATE = 'state';

    public function initialize(): void
    {
        parent::initialize();
        $this->InitiativesUsers = InitiativesUsersTable::load();
    }

    public function main($id = null, $secondParam = null)
    {
        if (!$this->OAuthServer->isManagerUser() && !$this->OAuthServer->isSmeUser() &&
            !$this->OAuthServer->isTrainerUser()) {
            throw new ForbiddenException('Resource not allowed with this token');
        }
        parent::main($id, $secondParam);
    }

    public function getList()
    {
        $filters = $this->processQueryFilters($this->request->getQueryParams());
        if ($this->OAuthServer->isSmeUser()) {
            $token = $this->request->getHeaders()['Authorization'][0] ?? '';
            $smeId = $this->OAuthServer->getUserID();
            $filters['user_ids'] = $this->OAuthServer->getUserIdsBySme($token, $smeId);
        }
        if ($this->OAuthServer->isTrainerUser()) {
            $token = $this->request->getHeaders()['Authorization'][0] ?? '';
            $parentId = $this->OAuthServer->getTrainerParent();
            $filters['user_ids'] = $this->OAuthServer->getUserIdsBySme($token, $parentId);
        }
        if ($filters['group'] ?? '' == self::GROUP_STATE) {
            $scorecard = $this->InitiativesUsers->getScorecardSummaryGroupedByState($filters);
        } else {
            $scorecard = $this->InitiativesUsers->getScorecardSummary($filters);
        }

        $this->return = $scorecard;
    }
}

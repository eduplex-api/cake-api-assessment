<?php

namespace Assessment\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property int tag
 */
class InitiativesTag extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'tag' => true,
    ];

    protected $_hidden = [];
}

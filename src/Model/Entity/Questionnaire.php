<?php

namespace Assessment\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property string $title
 * @property string $slug
 * @property string $language
 * @property int $related_questionnaire_id
 * @property string $signup_description
 * @property string $pdf_description_1
 * @property string $pdf_description_2
 * @property string $pdf_image
 * @property string $level_1
 * @property string $level_2
 * @property string $level_3
 * @property string $level_4
 * @property string $user_fields_component
 * @property boolean $hide_user_fields
 * @property string $user_submission_component
 * @property boolean $single_submit
 * @property string $checkbox_label
 * @property string $email_label
 * @property string $question_info
 * @property string $user_info
 */
class Questionnaire extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,
        'title' => true,
        'slug' => true,
        'type' => true,
        'related_questionnaire_id' => true,
        'selector_text' => true,
        'icon' => true,
        'signup_description' => true,
        'pdf_description_1' => true,
        'pdf_description_2' => true,
        'pdf_image' => true,
        'level_1' => true,
        'level_2' => true,
        'level_3' => true,
        'level_4' => true,
        'pdf_title' => true,
        'level_1_title' => true,
        'level_2_title' => true,
        'level_3_title' => true,
        'level_4_title' => true,
    ];

    protected $_hidden = [
        'created', 'modified', 'deleted',
    ];

    protected $_virtual = [
        'fields'
    ];

    protected function _getFields(): array
    {
        return $this->_fields['fields'] ?? [];
    }

    public function setFieldsQuestionnaire(array $fieldsQuestionnaires)
    {
        $this->_fields['fields_questionnaires'] = $fieldsQuestionnaires;
        $this->setDirty('fields_questionnaires');
    }

    public function getFieldsIdsList(): array
    {
        $fieldsList = [];
        /** @var Field $field */
        foreach ($this->_getFields() as $field) {
            $fieldsList[] = $field->id;
        }
        return $fieldsList;
    }
}

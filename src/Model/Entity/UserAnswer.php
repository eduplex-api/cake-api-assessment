<?php

namespace Assessment\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property int user_id
 * @property mixed manager_id
 * @property int questionnaire_id
 * @property int question_id
 * @property string question_text
 * @property mixed answer_id
 * @property string answer_text
 * @property mixed start_date
 *
 */
class UserAnswer extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,
        'question_id' => true,
        'question_text' => true,
        'answer_text' => true,
        'start_date' => true,
    ];

    protected $_hidden = [
        'created', 'modified', 'deleted',
    ];
}

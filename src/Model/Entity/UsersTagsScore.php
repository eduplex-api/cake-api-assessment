<?php
declare(strict_types=1);

namespace Assessment\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * @property int $user_id
 * @property int $tag
 * @property int $score
 * @property int $max_score
 * @property FrozenTime $created
 */
class UsersTagsScore extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'tag' => true,
        'score' => true,
        'max_score' => true
    ];

    protected $_hidden = [
        'deleted'
    ];

    protected $_virtual = [
    ];
}

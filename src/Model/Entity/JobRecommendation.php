<?php
declare(strict_types=1);

namespace Assessment\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * @property int $user_id
 * @property string $esco_uri
 * @property int $rating
 * @property string $category
 * @property float $distance
 */
class JobRecommendation extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,
        'esco_uri' => true,
        'rating' => true,
        'category' => true,
        'distance' => true,
    ];

    protected $_hidden = [
        'deleted'
    ];

    protected $_virtual = [
    ];
}

<?php

namespace Assessment\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property int question_id
 * @property string answer
 * @property int weight
 */
class Answer extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'answer' => true,
        'weight' => true,
    ];

    protected $_hidden = [
        'created', 'modified', 'deleted',
    ];
}

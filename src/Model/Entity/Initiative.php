<?php

namespace Assessment\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property int initiative_id
 * @property string tag
 * @property string $language
 * @property string $title
 * @property string $created
 * @property string $description
 * @property string[] $tags_list
 */
class Initiative extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'title' => true,
        'description' => true,
        'initiatives_tags' => true,
    ];

    protected $_hidden = [
        'language',
        'deleted',
        'initiatives_tags',
    ];

    protected $_virtual = [
        'tags_list'
    ];

    public function _getTagsList(): array
    {
        $tagList = [];
        /** @var InitiativesTag $initiativesTag */
        foreach ($this->getInitiativesTags() as $initiativesTag) {
            $tagList[] = $initiativesTag->tag;
        }
        return $tagList;
    }

    public function getInitiativesTags()
    {
        return $this->_fields['initiatives_tags'] ?? [];
    }

    public function setInitiativesTags(array $initiativesTags): void
    {
        $this->_fields['initiatives_tags'] = $initiativesTags;
        $this->setDirty('initiatives_tags');
    }
}

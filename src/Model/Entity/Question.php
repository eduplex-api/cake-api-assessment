<?php

namespace Assessment\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property string question
 * @property string tag
 * @property string language
 * @property string questionnaire_id
 */
class Question extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'question' => true,
        'tag' => true,
        'position' => true,
        'alt1' => true,
        'alt2' => true,
    ];

    protected $_hidden = [
        'language', 'created', 'modified', 'deleted',
    ];
}

<?php

namespace Assessment\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property int $id
 * @property int $field_id
 * @property int $questionnaire_id
 */
class FieldsQuestionnaire extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,
        'field_id' => true,
        'questionnaire_id' => true,
    ];

    protected $_hidden = [];
}

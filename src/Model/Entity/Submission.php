<?php
declare(strict_types=1);

namespace Assessment\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * @property int $user_id
 * @property string $job_position
 * @property string $occupation
 * @property string $working_times
 * @property boolean $home_office
 * @property string $location
 * @property int $questionnaire_id
 * @property FrozenTime $created
 * @property FrozenTime $modified
 * @property FrozenTime $deleted
 * @property Questionnaire $questionnaire
 * @property InitiativesUser[] $initiatives_users
 */
class Submission extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,
        'job_position' => true,
        'occupation' => true,
        'working_times' => true,
        'home_office' => true,
        'location' => true,
        'review' => true,
        'rating' => true,
    ];

    protected $_hidden = [
        'deleted'
    ];

    protected $_virtual = [
    ];
}

<?php

namespace Assessment\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property string $language
 * @property string $title
 */
class Field extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,
    ];

    protected $_hidden = [
        'language', '_joinData'
    ];

    protected $_virtual = [
    ];
}

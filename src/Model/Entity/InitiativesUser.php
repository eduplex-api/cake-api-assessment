<?php
declare(strict_types=1);

namespace Assessment\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * @property int $initiative_id
 * @property int $user_id
 * @property int $score
 * @property string $state
 * @property FrozenTime $created
 * @property mixed $max_score
 * @property mixed $percentage
 * @property mixed $is_flagged
 * @property int $questionnaire_id
 * @property mixed $manager_id
 * @property mixed $submission_id
 * @property mixed $deleted
 * @property Initiative $initiative
 */
class InitiativesUser extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'initiative_id' => true,
        'manager_id' => true,
        'score' => true,
        'max_score' => true,
        'is_flagged' => true,
        'state' => true,
        'created' => true,
    ];

    protected $_hidden = [
        'deleted'
    ];

    protected $_virtual = [
        'combined_amount'
    ];

    private $_combined = 1;

    public function getUser()
    {
        return $this->_fields['user'];
    }

    public function getInitiative(): Initiative
    {
        return $this->_fields['initiative'];
    }

    public function addCombinedAmount()
    {
        $this->_combined++;
    }

    public function _getCombinedAmount(): int
    {
        return $this->_combined;
    }
}

<?php

namespace Assessment\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Cake\Validation\Validator;
use RestApi\Lib\Validator\RestApiValidator;
use RestApi\Model\Table\RestApiTable;

/**
 * @property QuestionsTable $Questions
 */
class AnswersTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        QuestionsTable::addHasMany($this)->setForeignKey('question_id');
    }

    public static function load(): self
    {
        /** @var AnswersTable $table */
        $table = parent::load();
        return $table;
    }

    public function validationDefault(Validator $validator): Validator
    {
        /** @var RestApiValidator $validator */
        $validator
            ->requirePresence('answer', 'create')
            ->notEmptyString('answer')
            ->maxLength('answer', 255)
            ->requirePresence('weight', 'create')
            ->numeric('weight');
        return $validator;
    }

    public function findAnswersByQuestion($questionId)
    {
        return $this->find();
            //->where(['question_id' => $questionId]);
    }
}

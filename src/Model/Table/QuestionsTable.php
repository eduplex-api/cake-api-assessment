<?php

namespace Assessment\Model\Table;

use App\Lib\I18n\LegacyI18n;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\Validation\Validator;
use RestApi\Lib\Validator\RestApiValidator;
use RestApi\Model\Table\RestApiTable;

/**
 * @property QuestionnairesTable Questionnaires
 */
class QuestionsTable extends RestApiTable
{

    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        AnswersTable::addBelongsTo($this)->setForeignKey('question_id');
        QuestionnairesTable::addHasMany($this)->setForeignKey('questionnaire_id');
    }

    public static function load(): self
    {
        /** @var QuestionsTable $table */
        $table = parent::load();
        return $table;
    }

    public function validationDefault(Validator $validator): Validator
    {
        /** @var RestApiValidator $validator */
        $validator
            ->requirePresence('question', 'create')
            ->notEmptyString('question')
            ->requirePresence('tag', 'create')
            ->notEmptyString('tag')
            ->maxLength('tag', 100);
        return $validator;
    }

    public function findQuestionsByQuestionnaireWithAnswers($questionnaireId, $filters): Query
    {
        $lang = LegacyI18n::getLocale();
        $query = $this->find()->where([
            'language' => $lang,
            'questionnaire_id' => $questionnaireId
        ])->contain('Answers');
        $this->_applyOrder($filters, $query);
        return $query;
    }

    private function _applyOrder(array $filters, Query $query)
    {
        switch ($filters['sort_by'] ?? '') {
            case 'position':
                $orderOptions = ['Questions.position' => $filters['order'] ?? 'asc'];
                break;
            default:
                $orderOptions = [
                    'Questions.id' => 'asc',
                    'Questions.position' => 'asc',
                ];
        }
        $query->order($orderOptions);
    }
}

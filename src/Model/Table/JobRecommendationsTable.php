<?php
declare(strict_types=1);

namespace Assessment\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use RestApi\Model\Table\RestApiTable;

class JobRecommendationsTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
    }

    public static function load(): self
    {
        /** @var self $table */
        $table = parent::load();
        return $table;
    }

    public function findRecommendationsByUser($userId): Query
    {
        return $this->find()->where(['user_id' => $userId]);
    }
}

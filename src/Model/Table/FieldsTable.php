<?php

namespace Assessment\Model\Table;

use App\Lib\I18n\LegacyI18n;
use Cake\ORM\Query;
use RestApi\Model\Table\RestApiTable;

class FieldsTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->belongsToMany('Questionnaires',
            [
                'className' => QuestionnairesTable::nameWithPlugin(),
                'joinTable' => \Assessment\AssessmentPlugin::getTablePrefix() . 'fields_questionnaires'
            ])
            ->setForeignKey('field_id');
    }

    public static function load(): self
    {
        /** @var FieldsTable $table */
        $table = parent::load();
        return $table;
    }

    public function findAllFields(): Query
    {
        $lang = LegacyI18n::getLocale();
        return $this->find()
            ->where(['language' => $lang])
            ->order(['position' => 'asc']);
    }

}

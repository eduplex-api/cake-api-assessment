<?php

namespace Assessment\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\Validation\Validator;
use RestApi\Lib\Validator\RestApiValidator;
use RestApi\Model\Table\RestApiTable;

class UserAnswersTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        QuestionnairesTable::addHasMany($this)->setForeignKey('questionnaire_id');
        UsersTable::addHasMany($this)->setForeignKey('userID');
        UsersTable::addHasMany($this)->setForeignKey('manager_id');
        QuestionsTable::addHasMany($this)->setForeignKey('question_id');
        AnswersTable::addHasMany($this)->setForeignKey('answer_id');
    }

    public static function load(): self
    {
        /** @var UserAnswersTable $table */
        $table = parent::load();
        return $table;
    }

    public function validationDefault(Validator $validator): Validator
    {
        /** @var RestApiValidator $validator */
        $validator
            ->requirePresence('question_text', 'create')
            ->requirePresence('answer_text', 'create')
            ->requirePresence('start_date', 'create');
        return $validator;
    }
}

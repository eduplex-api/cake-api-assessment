<?php
declare(strict_types=1);

namespace Assessment\Model\Table;

use App\Model\Table\UsersTable;
use Assessment\Lib\Consts\InitiativesUsersStates;
use Assessment\Model\Entity\Initiative;
use Assessment\Model\Entity\InitiativesUser;
use Assessment\Model\Entity\Questionnaire;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\Validation\Validator;
use RestApi\Lib\Validator\RestApiValidator;
use RestApi\Model\Table\RestApiTable;

/**
 * @property InitiativesTable $Initiatives
 * @property UsersTable $Users
 * @property QuestionnairesTable $Questionnaires
 */
class InitiativesUsersTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        InitiativesTable::addHasMany($this)->setForeignKey('initiative_id');
        UsersTable::addHasMany($this)->setForeignKey('user_id');
        QuestionnairesTable::addHasMany($this)->setForeignKey('questionnaire_id');
    }

    public static function load(): self
    {
        /** @var InitiativesUsersTable $table */
        $table = parent::load();
        return $table;
    }

    public function validationDefault(Validator $validator): Validator
    {
        /** @var RestApiValidator $validator */
        $validator
            ->inList('state', [
                InitiativesUsersStates::IN_PROGRESS,
                InitiativesUsersStates::COMPLETED,
                InitiativesUsersStates::NOT_STARTED,
            ], 'Invalid state')
            ->allowEmptyFor('state');
        return $validator;
    }

    public function findLastInitiativesUsersByUser($userId, $questionnaireId, $filters = []): Query
    {
        $query = $this->find()
            ->where([
                'user_id' => $userId,
                'questionnaire_id' => $questionnaireId,
            ])
            ->contain('Initiatives.InitiativesTags');

        return $this->_applyFilters($query, $filters);
    }

    public function getLastInitiativesUsersByUser($userId, $questionnaireId, $filters = []): array
    {
        $query = $this->find()->where([
            'user_id' => $userId,
            'questionnaire_id' => $questionnaireId,
        ])
            ->contain('Initiatives.InitiativesTags');

        $initiativesUser = $this->_applyFilters($query, $filters)->toArray();


        /** @var Questionnaire $questionnaire */
        $questionnaire = $this->Questionnaires->get($questionnaireId);
        if ($questionnaire->related_questionnaire_id) {
            $relatedInitiativesUser = $this->find()->where([
                'user_id' => $userId,
                'questionnaire_id' => $questionnaire->related_questionnaire_id,
            ])->toArray();

            /** @var InitiativesUser $relatedInitiativeUser */
            foreach ($relatedInitiativesUser ?? [] as $relatedInitiativeUser) {
                /** @var InitiativesUser $initiativeUser */
                foreach ($initiativesUser as $initiativeUser) {
                    if ($initiativeUser->initiative_id === $relatedInitiativeUser->initiative_id) {
                        if ($initiativeUser->score > 0) {
                            $initiativeUser->addCombinedAmount();
                            $initiativeUser->max_score += $relatedInitiativeUser->max_score;
                            $initiativeUser->score += $relatedInitiativeUser->score;
                            $initiativeUser->percentage = ($initiativeUser->percentage +
                                    $relatedInitiativeUser->percentage) / 2;
                        } else {
                            $initiativeUser->max_score = $relatedInitiativeUser->max_score;
                            $initiativeUser->score = $relatedInitiativeUser->score;
                            $initiativeUser->percentage = $relatedInitiativeUser->percentage;
                        }
                    }
                }
            }
        }
        return $initiativesUser;
    }

    public function findByIdWithInitiatives($id): Query
    {
        return $this->findById($id)->contain('Initiatives');
    }

    public function getScorecardSummary($filters = []): array
    {
        $initiativesFields = $this->Initiatives->getFields();
        $query = $this->find();
        if (isset($filters['tags'])) {
            $initiativeIds = $this->_getInitiativeIdsFromTagList($filters['tags']);
            if (empty($initiativeIds)) {
                return [];
            }
            $query->where(['InitiativesUsers.initiative_id in' => $initiativeIds]);
        }
        $query
            ->select([
                'avg_percentage' => $query->func()->avg('InitiativesUsers.percentage'),
                'InitiativesUsers.initiative_id',
            ])
            ->select($initiativesFields)
            ->group(['InitiativesUsers.initiative_id'])
            ->contain('Initiatives.InitiativesTags');

        $query = $this->_applyFilters($query, $filters);

        $qResult = $query->all();
        $summary = [];
        /** @var InitiativesUser $initiativeUser */
        foreach ($qResult as $initiativeUser) {
            $summary[] = [
                'avg_percentage' => round($initiativeUser->avg_percentage),
                'initiative_id' => $initiativeUser->initiative_id,
                'initiative' => $initiativeUser->initiative,
            ];
        }
        return $summary;
    }

    public function getScorecardSummaryGroupedByState($filters = []): array
    {
        $initiativesFields = $this->Initiatives->getFields();
        $query = $this->find();
        $query
            ->select([
                'count_users' => $query->func()->count('InitiativesUsers.user_id'),
                'avg_percentage' => $query->func()->avg('InitiativesUsers.percentage'),
                'InitiativesUsers.state', 'InitiativesUsers.initiative_id'
            ])
            ->select($initiativesFields)
            ->where(['InitiativesUsers.state is not' => null])
            ->order(['InitiativesUsers.initiative_id', 'InitiativesUsers.state'])
            ->group(['InitiativesUsers.initiative_id', 'InitiativesUsers.state'])
            ->contain('Initiatives.InitiativesTags');

        $query = $this->_applyFilters($query, $filters);
        $qResult = $query->all();
        $summary = [];
        /** @var InitiativesUser $initiativeUser */
        foreach ($qResult as $initiativeUser) {
            if (!isset($summary[$initiativeUser->initiative_id])) {
                $summary[$initiativeUser->initiative_id] = [
                    InitiativesUsersStates::NOT_STARTED => 0,
                    InitiativesUsersStates::IN_PROGRESS => 0,
                    InitiativesUsersStates::COMPLETED => 0,
                    'avg_not_started' => 0,
                    'avg_in_progress' => 0,
                    'avg_completed' => 0,
                    'initiative' => [
                        'id' => $initiativeUser->getInitiative()->id,
                        'title' => $initiativeUser->getInitiative()->title,
                        'created' => $initiativeUser->getInitiative()->created,
                        'tags_list' => $initiativeUser->getInitiative()->_getTagsList()
                    ],
                ];
            }
            $summary[$initiativeUser->initiative_id][$initiativeUser->state] =
                $initiativeUser->count_users;
            $summary[$initiativeUser->initiative_id]['avg_' . $initiativeUser->state] =
                $initiativeUser->avg_percentage;
        }
        return array_values($summary);
    }

    public function getDashboardTopPerformers($filters = [], $limit = 3): array
    {
        $userFields = $this->Users->getFields();
        $query = $this->_applyFilters($this->find(), $filters);
        $query
            ->select([
                'avg_percentage' => $query->func()->avg('InitiativesUsers.percentage'),
                'InitiativesUsers.user_id'
            ])
            ->select($userFields)
            ->group(['InitiativesUsers.user_id'])
            ->contain('Users')
            ->order(['avg_percentage desc'])
            ->limit($limit);

        $qResult = $query->all();
        $topPerformers = [];
        /** @var InitiativesUser $initiativeUser */
        foreach ($qResult as $initiativeUser) {
            $topPerformers[] = [
                'avg_percentage' => $initiativeUser->avg_percentage,
                'user' => $initiativeUser->getUser(),
            ];
        }
        return $topPerformers;
    }

    public function getDashboardTotalRespondentsByState($filters = []): array
    {
        $query = $this->_applyFilters($this->find(), $filters);
        $query->select([
            'amount' => $query->func()->count('InitiativesUsers.id'),
        ])
            ->where(['InitiativesUsers.state is not NULL'])
            ->select(['state'])
            ->group(['InitiativesUsers.state'])
            ->orderAsc('state');
        $qResult = $query->all();

        $result = [];
        /** @var InitiativesUser $initiativeUser */
        foreach ($qResult as $initiativeUser) {
            $result[$initiativeUser->state] = [
                'state' => $initiativeUser->state,
                'amount' => $initiativeUser->amount,
            ];
        }
        return array_values($result);
    }

    public function getDashboardTotalRespondentsByScore($filters = []): array
    {
        $q = $this->_applyFilters($this->find(), $filters);
        return [
            'range-1' => $q->cleanCopy()->where(['percentage <' => 30])->count(),
            'range-2' => $q->cleanCopy()->where(['percentage >=' => 30, 'percentage <' => 50])->count(),
            'range-3' => $q->cleanCopy()->where(['percentage >=' => 50, 'percentage <' => 65])->count(),
            'range-4' => $q->cleanCopy()->where(['percentage >=' => 65, 'percentage <' => 80])->count(),
            'range-5' => $q->cleanCopy()->where(['percentage >=' => 80, 'percentage <' => 90])->count(),
            'range-6' => $q->cleanCopy()->where(['percentage >=' => 90, 'percentage <' => 95])->count(),
            'range-7' => $q->cleanCopy()->where(['percentage >=' => 95])->count(),
        ];
    }

    public function getDashboardTotalRespondentsByScoreGroupedByInitiative($filters = []): array
    {
        $ranges = ['range-1', 'range-2', 'range-3', 'range-4', 'range-5'];
        $tagList = $filters['tags'] ?? null;
        $initiatives = $this->Initiatives->findAllInitiativesByTagList($tagList);
        $initiativeIds = [];
        foreach ($initiatives as $initiative) {
            $initiativeIds[] = $initiative->id;
        }
        $q = $this->_applyFilters($this->find(), $filters);
        $initiativesUsersGrouped = [
            $ranges[0] => $this->_applyGroup($q, $initiativeIds)
                ->where(['percentage' => 0])->toArray(),
            $ranges[1] => $this->_applyGroup($q, $initiativeIds)
                ->where(['percentage >' => 0, 'percentage <' => 50])->toArray(),
            $ranges[2] => $this->_applyGroup($q, $initiativeIds)
                ->where(['percentage >=' => 50, 'percentage <' => 70])->toArray(),
            $ranges[3] => $this->_applyGroup($q, $initiativeIds)
                ->where(['percentage >=' => 70, 'percentage <' => 90])->toArray(),
            $ranges[4] => $this->_applyGroup($q, $initiativeIds)
                ->where(['percentage >=' => 90])->toArray(),
        ];
        $summary = [];
        /** @var Initiative $initiative */
        foreach ($initiatives as $initiative) {
            foreach ($initiative->_getTagsList() as $tag) {
                if (!isset($summary[$tag])) {
                    $summary[$tag] = [
                        $ranges[0] => 0,
                        $ranges[1] => 0,
                        $ranges[2] => 0,
                        $ranges[3] => 0,
                        $ranges[4] => 0,
                    ];
                }
                /** @var InitiativesUser $initiativeUser */
                foreach ($ranges as $range) {
                    foreach ($initiativesUsersGrouped[$range] as $initiativeUser) {
                        if ($initiativeUser->initiative_id == $initiative->id) {
                            $summary[$tag][$range] += $initiativeUser->count;
                        }
                    }
                }
            }
        }
        return $summary;
    }

    private function _applyFilters(Query $query, array $filters): Query
    {
        if (isset($filters['date:gte'])) {
            $query->where([
                'InitiativesUsers.created >=' => $filters['date:gte'],
            ]);
        }
        if (isset($filters['date:lte'])) {
            $query->where([
                'InitiativesUsers.created <=' => $filters['date:lte'],
            ]);
        }
        if (isset($filters['user_ids'])) {
            if (empty($filters['user_ids'])) {
                $query->where([
                    'InitiativesUsers.user_id' => 0,
                ]);
            } else {
                $query->where([
                    'InitiativesUsers.user_id IN' => $filters['user_ids'],
                ]);
            }
        }
        if (isset($filters['questionnaire_id'])) {
            $query->where(['InitiativesUsers.questionnaire_id' => $filters['questionnaire_id']]);
        }
        if (isset($filters['manager_id'])) {
            $query->where(['InitiativesUsers.manager_id' => $filters['manager_id']]);
        }
        if (isset($filters['submission_id'])) {
            $query->where(['InitiativesUsers.submission_id' => $filters['submission_id']])
                ->withDeleted(true);
        }
        if (isset($filters['historic'])) {
            $query->withDeleted(true);
        }
        return $query;
    }

    private function _applyGroup(Query $q, array $initiativeIds): Query
    {
        return $q->cleanCopy()
            ->where(['initiative_id in' => $initiativeIds])
            ->select([
                'InitiativesUsers.initiative_id',
                'count' => $q->func()->count('InitiativesUsers.id'),
            ])
            ->group(['InitiativesUsers.initiative_id']);
    }

    private function _getInitiativeIdsFromTagList($tagList): array
    {
        $initiatives = $this->Initiatives->findAllInitiativesByTagList($tagList);
        $initiativeIds = [];
        foreach ($initiatives as $initiative) {
            $initiativeIds[] = $initiative->id;
        }
        return $initiativeIds;
    }

    public function getUserIdsByAnsweredQuestionnaires($qAnswered, $qUnanswered): array
    {
        $usersAnsweredQ1 = $this->_getUserByQuestionnaireAnswered($qAnswered);
        $usersAnsweredQ2 = $this->_getUserByQuestionnaireAnswered($qUnanswered);
        return array_values(array_diff($usersAnsweredQ1, $usersAnsweredQ2));
    }

    public function getUserScoreInTags($userId, $tags): array
    {
        /** @var InitiativesUser $initiativeUser */
        $initiativeUsers = $this->find()
            ->where(['InitiativesUsers.user_id' => $userId])
            ->contain('Initiatives.InitiativesTags')
            ->innerJoinWith('Initiatives.InitiativesTags', function (Query $q) use ($tags) {
                return $q->where(['InitiativesTags.tag in' => $tags]);
            })
        ->toArray();
        $scores = [];
        /** @var InitiativesUser $initiativeUser */
        foreach ($initiativeUsers as $initiativeUser) {
            $tag = $initiativeUser->getInitiative()->getInitiativesTags()[0]->tag ?? null;
            if ($tag) {
                $scores[$tag] = [
                    'score' => $initiativeUser->score ?? null,
                    'max_score' => $initiativeUser->max_score ?? null
                ];
            }
        }
        return $scores;
    }

    private function _getUserByQuestionnaireAnswered($questionnaireId): array
    {
        $initiativesUsers = $this->find()
            ->where(['questionnaire_id' => $questionnaireId])
            ->select('user_id')
            ->group('user_id')
            ->toArray();
        $userIds = [];
        /** @var InitiativesUser $initiativeUser */
        foreach ($initiativesUsers as $initiativeUser) {
            $userIds[] = $initiativeUser->user_id;
        }
        return $userIds;
    }
}

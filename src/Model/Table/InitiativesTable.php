<?php

namespace Assessment\Model\Table;

use App\Lib\I18n\LegacyI18n;
use Assessment\Model\Entity\Initiative;
use Assessment\Model\Entity\InitiativesTag;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\Validation\Validator;
use Closure;
use RestApi\Lib\Validator\RestApiValidator;
use RestApi\Model\Table\RestApiTable;

/**
 * @property \Cake\ORM\Association InitiativesTags
 */
class InitiativesTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        InitiativesTagsTable::addBelongsTo($this)->setForeignKey('initiative_id');
    }

    public function validationDefault(Validator $validator): Validator
    {
        /** @var RestApiValidator $validator */
        $validator
            ->requirePresence('title', 'create')
            ->notEmptyString('title');
        return $validator;
    }

    public static function load(): self
    {
        /** @var InitiativesTable $table */
        $table = parent::load();
        return $table;
    }

    public function findAllInitiativesWithTags(): Query
    {
        $lang = LegacyI18n::getLocale();
        $query = $this->find()
            ->where(['language' => $lang])
            ->contain('InitiativesTags');
        return $query;
    }

    public function findAllInitiativesByTagList($tagList): Query
    {
        return $this->findAllInitiativesWithTags()
            ->innerJoinWith('InitiativesTags',
                $this->_getInitiativesTagsFilterByTagList($tagList))
            ->contain('InitiativesTags',
                $this->_getInitiativesTagsFilterByTagList($tagList));
    }

    public function findByIdWithTags($id): Query
    {
        return $this->findById($id)->contain('InitiativesTags');
    }

    public function manageInitiativeTags(Initiative $initiative, $newAssociatedTags)
    {
        $tagsToRemove = array_diff($initiative->_getTagsList(), $newAssociatedTags);
        $tagsToAdd = array_diff($newAssociatedTags, $initiative->_getTagsList());
        if ($tagsToRemove) {
            $this->removeTagsFromInitiative($initiative->id, $tagsToRemove);
        }
        if ($tagsToAdd) {
            $this->addTagsToInitiative($initiative->id, $tagsToAdd);
        }
        return $newAssociatedTags;
    }

    public function removeTagsFromInitiative($initiativeId, $tags): int
    {
        return $this->InitiativesTags->deleteAll([
            'initiative_id' => $initiativeId,
            'tag in' => $tags,
        ]);
    }

    public function addTagsToInitiative($initiativeId, $tags): array
    {
        $toSave = [];
        foreach ($tags as $tag) {
            $newInitiativesTag = new InitiativesTag([
                'initiative_id' => $initiativeId,
                'tag' => $tag
            ]);
            $toSave[] = $newInitiativesTag;
        }
        $res = $this->InitiativesTags->saveMany($toSave);
        return $res;
    }

    private function _getInitiativesTagsFilterByTagList($tagList): Closure
    {
        return function (Query $q) use ($tagList) {
            if ($tagList) {
                return $q->where(['InitiativesTags.tag in' => $tagList]);
            }
            return $q;
        };
    }
}

<?php
declare(strict_types=1);

namespace Assessment\Model\Table;

use App\Lib\I18n\LegacyI18n;
use Assessment\Model\Entity\FieldsQuestionnaire;
use Assessment\Model\Entity\Questionnaire;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use RestApi\Model\Table\RestApiTable;

/**
 * @property \Cake\ORM\Association FieldsQuestionnaires
 */
class QuestionnairesTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        $this->hasOne('RelatedQuestionnaire', [
            'className' => QuestionnairesTable::nameWithPlugin()
        ])->setForeignKey('ralated_questionnaire_id');
        FieldsQuestionnairesTable::addBelongsTo($this)->setForeignKey('questionnaire_id');
        InitiativesUsersTable::addBelongsTo($this)->setForeignKey('questionnaire_id');
        $this->belongsToMany('Fields',
            [
                'className' => FieldsTable::nameWithPlugin(),
                'joinTable' => \Assessment\AssessmentPlugin::getTablePrefix() . 'fields_questionnaires',
                'foreignKey' => 'questionnaire_id',
                'targetForeignKey' => 'field_id',
            ]);
    }

    public static function load(): self
    {
        /** @var QuestionnairesTable $table */
        $table = parent::load();
        return $table;
    }

    public function findAllWithFields(): Query
    {
        $lang = LegacyI18n::getLocale();
        return $this->find()
            ->where(['Questionnaires.language' => $lang])
            ->contain('Fields', $this->_getFieldsLangFilter());
    }

    public function findAnsweredByUser($userId): Query
    {
        $lang = LegacyI18n::getLocale();
        return $this->find()
            ->where(['Questionnaires.language' => $lang])
            ->innerJoinWith('InitiativesUsers', function (Query $q) use ($userId){
                return $q->where(['user_id' => $userId]);
            })->distinct('Questionnaires.id');
    }

    public function findBySlugWithFields($slug): Query
    {
        $lang = LegacyI18n::getLocale();
        $query = $this->find()
            ->where([
                'Questionnaires.slug' => $slug,
                'Questionnaires.language' => $lang,
            ])->contain('Fields', $this->_getFieldsLangFilter());
        return $query;
    }

    public function findByIdWithFields($id)
    {
        $lang = LegacyI18n::getLocale();
        $query = $this->findById($id)
            ->where(['Questionnaires.language' => $lang])
            ->contain('Fields', $this->_getFieldsLangFilter());
        return $query;
    }

    public function manageFieldsQuestionnaire(Questionnaire $questionnaire, $newAssociatedFields)
    {
        $fieldsToRemove = array_diff($questionnaire->getFieldsIdsList(), $newAssociatedFields);
        $fieldsToAdd = array_diff($newAssociatedFields, $questionnaire->getFieldsIdsList());
        if ($fieldsToRemove) {
            $this->removeFieldsFromQuestionnaire($questionnaire->id, $fieldsToRemove);
        }
        if ($fieldsToAdd) {
            $this->addFieldsToQuestionnaire($questionnaire->id, $fieldsToAdd);
        }
        return $newAssociatedFields;
    }

    public function removeFieldsFromQuestionnaire($questionnaireId, $fields): int
    {
        return $this->FieldsQuestionnaires->deleteAll([
            'questionnaire_id' => $questionnaireId,
            'field_id in' => $fields,
        ]);
    }

    public function addFieldsToQuestionnaire($questionnaire_id, $fields): array
    {
        $toSave = [];
        foreach ($fields as $field_id) {
            $newFieldsQuestionnaire = new FieldsQuestionnaire([
                'questionnaire_id' => $questionnaire_id,
                'field_id' => $field_id
            ]);
            $toSave[] = $newFieldsQuestionnaire;
        }
        return $this->FieldsQuestionnaires->saveMany($toSave);
    }

    private function _getFieldsLangFilter(): \Closure
    {
        $lang = LegacyI18n::getLocale();
        return function (Query $q) use ($lang) {
            return $q->where(['Fields.language' => $lang]);
        };
    }

}

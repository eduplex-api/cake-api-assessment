<?php
declare(strict_types=1);

namespace Assessment\Model\Table;

use Assessment\Model\Entity\Submission;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\Validation\Validator;
use RestApi\Lib\Validator\RestApiValidator;
use RestApi\Model\Table\RestApiTable;

class SubmissionsTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);

        QuestionnairesTable::addHasMany($this)->setForeignKey('questionnaire_id');
        InitiativesUsersTable::addBelongsTo($this)->setForeignKey('submission_id');
    }

    public static function load(): self
    {
        /** @var self $table */
        $table = parent::load();
        return $table;
    }

    public function validationDefault(Validator $validator): Validator
    {
        /** @var RestApiValidator $validator */
        $validator
            ->numeric('rating');
        return $validator;
    }

    public function findSubmissionsByUser($userId): Query
    {
        return $this->find()
            ->where(['user_id' => $userId])
            ->contain('Questionnaires');
    }

    private function findVcResults($userId): Query
    {
        return $this->find()
            ->where(['user_id' => $userId])
            ->contain('Questionnaires')
            ->contain('InitiativesUsers.Initiatives.InitiativesTags')
            ->orderDesc(1);
    }

    public function getLastVcResults($userId): Submission
    {
        /** @var Submission $res */
        $res = $this->findVcResults($userId)
            ->firstOrFail();
        return $res;
    }
}

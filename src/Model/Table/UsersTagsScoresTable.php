<?php
declare(strict_types=1);

namespace Assessment\Model\Table;

use App\Model\Table\UsersTable;
use Assessment\Model\Entity\UsersTagsScore;
use Cake\I18n\FrozenTime;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use RestApi\Model\Table\RestApiTable;

class UsersTagsScoresTable extends RestApiTable
{
    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
        UsersTable::addHasMany($this)->setForeignKey('user_id');
    }

    public static function load(): self
    {
        /** @var self $table */
        $table = parent::load();
        return $table;
    }

    public function findByUserAndTag($userId, $tag): Query
    {
        return $this->find()
            ->where([
                'user_id' => $userId,
                'tag' => $tag
            ]);
    }

    public function softDeleteOldUserScores(UsersTagsScore $newScore): int
    {
        return $this->updateAll(['deleted' => new FrozenTime()],
            [
                'user_id' => $newScore->user_id,
                'tag' => $newScore->tag,
                'id !=' => $newScore->id
            ]
        );
    }

    public function getUserScoreInTags($userId, array $tagSlugs): array
    {
        $scores = $this->find()
            ->where([
                'user_id' => $userId,
                'tag in' => $tagSlugs
            ]);
        $toRet = [];
        /** @var UsersTagsScore $uScore */
        foreach ($scores as $uScore) {
            $toRet[$uScore->tag] = [
                'score' => $uScore->score,
                'max_score' => $uScore->max_score,
            ];
        }
        return $toRet;
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeUserAnswersAddManagerId extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'user_answers');
        $table->addColumn('manager_id', 'integer', [
            'null' => true,
            'after' => 'user_id'
        ])->update();
    }
}

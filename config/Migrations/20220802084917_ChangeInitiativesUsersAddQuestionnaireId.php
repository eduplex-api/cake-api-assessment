<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeInitiativesUsersAddQuestionnaireId extends AbstractMigration
{
    public function change()
    {
        $initiativesUsers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives_users');
        $initiativesUsers->addColumn('questionnaire_id', 'integer', [
            'null' => true,
            'after' => 'user_id'
        ])->update();
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateUserAnswers extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'user_answers');
        $table->addColumn('user_id', 'integer', [
            'null' => false,
        ])->addColumn('questionnaire_id', 'integer', [
            'null' => false,
        ])->addColumn('question_id', 'integer', [
            'null' => false,
        ])->addColumn('question_text', 'text', [
            'limit' => MysqlAdapter::TEXT_MEDIUM,
            'null' => false,
        ])->addColumn('answer_id', 'integer', [
            'null' => false,
        ])->addColumn('answer_text', 'text', [
            'limit' => MysqlAdapter::TEXT_MEDIUM,
            'null' => false,
        ])->addColumn('start_date', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}

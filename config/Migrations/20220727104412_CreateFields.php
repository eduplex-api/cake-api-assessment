<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'fields',
            ['collation' => 'utf8mb4_unicode_ci']);
        $table->addColumn('title', 'string', [
            'limit' => MysqlAdapter::TEXT_SMALL,
            'null' => false,
        ])->addColumn('icon', 'string', [
            'limit' => MysqlAdapter::TEXT_SMALL,
            'null' => true,
        ])->addColumn('language', 'string', [
            'limit' => MysqlAdapter::TEXT_SMALL,
            'null' => true,
        ])->addIndex(['title']);
        $table->create();
    }
}

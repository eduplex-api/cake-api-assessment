<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeQuestionsAddPosition extends AbstractMigration
{
    public function change()
    {
        $questions = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questions');
        $questions->addColumn('position', 'integer', [
            'null' => true,
            'default' => null,
            'after' => 'tag'
        ])->update();
    }
}

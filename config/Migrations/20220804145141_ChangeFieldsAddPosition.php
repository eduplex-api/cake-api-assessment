<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeFieldsAddPosition extends AbstractMigration
{
    public function change()
    {
        $initiativesUsers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'fields');
        $initiativesUsers->addColumn('position', 'integer', [
            'null' => true,
            'after' => 'language'
        ])->update();
    }
}

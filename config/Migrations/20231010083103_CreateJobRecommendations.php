<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateJobRecommendations extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'job_recommendations');
        $table->addColumn('user_id', 'integer', [
            'null' => false,
        ])->addColumn('esco_uri', 'string', [
            'null' => true,
        ])->addColumn('rating', 'integer', [
            'null' => true,
        ])->addColumn('category', 'string', [
            'null' => true,
        ])->addColumn('distance', 'float', [
            'null' => true,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex('user_id');
        $table->create();
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateInitiativesTags extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives_tags',
            ['collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('initiative_id', 'integer', [
                'null' => false,])
            ->addColumn('tag', 'string', [
                'null' => false,]);
        $table->create();
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateQuestions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questions',
            ['collation'=>'utf8mb4_unicode_ci']);
        $table->addColumn('question', 'text', [
            'limit' => MysqlAdapter::TEXT_MEDIUM,
            'null' => false,
            //'encoding' => 'utf8mb4'
        ])->addColumn('tag', 'string', [
            'limit' => 100,
            'null' => false,
        ])->addColumn('language', 'string', [
            'limit' => 10,
            'null' => false,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateInitiatives extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives',
            ['collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('title', 'string', [
                'limit' => MysqlAdapter::TEXT_TINY,
                'null' => false])
            ->addColumn('description', 'text', [
                'limit' => MysqlAdapter::TEXT_REGULAR,
                'null' => true])
            ->addColumn('points', 'integer', [
                'limit' => 10,
                'null' => false])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => true])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => true])
            ->addColumn('deleted', 'datetime', [
                'default' => null,
                'null' => true,
            ]);
        $table->create();
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeQuestionnairesAddTypeRelated extends AbstractMigration
{
    public function change()
    {
        $initiativesUsers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires');
        $initiativesUsers->addColumn('type', 'string', [
            'null' => true,
            'after' => 'language'
        ])->addColumn('related_questionnaire_id', 'integer', [
            'null' => true,
            'after' => 'type'
        ])->addIndex('related_questionnaire_id')->update();
    }
}

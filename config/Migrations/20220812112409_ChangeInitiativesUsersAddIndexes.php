<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeInitiativesUsersAddIndexes extends AbstractMigration
{
    public function change()
    {
        $initiativesUsers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives_users');
        $initiativesUsers->addIndex([
            'questionnaire_id',
            'initiative_id',
            'manager_id'
        ])->update();
    }
}

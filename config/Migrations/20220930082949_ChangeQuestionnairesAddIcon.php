<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeQuestionnairesAddIcon extends AbstractMigration
{
    public function change()
    {
        $questionnaires = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires');
        $questionnaires->addColumn('icon', 'string', [
            'null' => true,
            'after' => 'type'
        ])->update();
    }
}

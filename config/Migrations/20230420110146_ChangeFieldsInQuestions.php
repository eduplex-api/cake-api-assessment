<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class ChangeFieldsInQuestions extends AbstractMigration
{
    public function change()
    {
        $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questions')
            ->changeColumn('alt1', 'text', [
                'limit' => MysqlAdapter::TEXT_MEDIUM,
                'null' => true
            ])
            ->changeColumn('alt2', 'text', [
                'limit' => MysqlAdapter::TEXT_MEDIUM,
                'null' => true
            ])
            ->update();
    }
}

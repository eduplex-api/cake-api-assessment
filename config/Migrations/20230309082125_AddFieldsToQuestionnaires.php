<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddFieldsToQuestionnaires extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires');
        $table->addColumn('user_fields_component', 'string', [
            'null' => true,
            'after' => 'level_4'
        ])->addColumn('hide_user_fields', 'boolean', [
            'null' => true,
            'after' => 'user_fields_component'
        ])->addColumn('user_submission_component', 'string', [
            'null' => true,
            'after' => 'hide_user_fields'
        ])->addColumn('single_submit', 'boolean', [
            'null' => true,
            'after' => 'user_submission_component'
        ]);
        $table->update();
    }
}

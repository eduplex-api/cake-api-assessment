<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeInitiativesUsersAddManagerId extends AbstractMigration
{
    public function change()
    {
        $initiativesUsers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives_users');
        $initiativesUsers->addColumn('manager_id', 'integer', [
            'null' => true,
            'after' => 'questionnaire_id'
        ])->update();
    }
}

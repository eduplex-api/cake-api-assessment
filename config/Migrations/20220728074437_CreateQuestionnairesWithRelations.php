<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateQuestionnairesWithRelations extends AbstractMigration
{
    public function change()
    {
        $questionnatires = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires',
            ['collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('title', 'string', [
                'limit' => MysqlAdapter::TEXT_SMALL,
                'null' => false])
            ->addColumn('slug', 'string', [
                'limit' => MysqlAdapter::TEXT_SMALL,
                'null' => false])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => true])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => true])
            ->addColumn('deleted', 'datetime', [
                'default' => null,
                'null' => true,])
            ->addIndex(['slug']);
        $questionnatires->create();

        $questionnairesFields =  $this->table(
            \Assessment\AssessmentPlugin::getTablePrefix() .  'fields_questionnaires',
            ['collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('field_id', 'integer', [
                'limit' => MysqlAdapter::INT_REGULAR,
                'null' => false])
            ->addColumn('questionnaire_id', 'integer', [
                'limit' => MysqlAdapter::INT_REGULAR,
                'null' => false])
            ->addIndex(['field_id', 'questionnaire_id']);
        $questionnairesFields->create();

        $questions = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questions');
        $questions->addColumn('questionnaire_id', 'integer', [
            'null' => true,
            'default' => null,
            'after' => 'tag'
        ])->update();

        $fields = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'fields');
        $fields->addColumn('slug', 'string', [
            'null' => false,
            'after' => 'title'
        ])->update();
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeInitiativesUsersAddIsFlagged extends AbstractMigration
{
    public function change()
    {
        $initiativesUsers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives_users');
        $initiativesUsers->addColumn('is_flagged', 'boolean', [
            'null' => false,
            'default' => false,
            'after' => 'state'
        ])->update();
    }
}

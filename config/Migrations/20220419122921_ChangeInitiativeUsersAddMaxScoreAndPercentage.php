<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeInitiativeUsersAddMaxScoreAndPercentage extends AbstractMigration
{
    public function change()
    {
        $initiatives = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives_users');
        $initiatives->addColumn('max_score', 'integer', [
            'null' => true,
            'after' => 'score'
        ])->addColumn('percentage', 'integer', [
            'null' => true,
            'after' => 'max_score'
        ])->update();
    }
}

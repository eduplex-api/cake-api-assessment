<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeQuestionnairesAddLanguage extends AbstractMigration
{
    public function change()
    {
        $initiativesUsers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires');
        $initiativesUsers->addColumn('language', 'string', [
            'null' => true,
            'after' => 'slug'
        ])->update();
    }
}

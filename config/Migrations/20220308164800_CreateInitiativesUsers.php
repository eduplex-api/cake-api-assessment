<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateInitiativesUsers extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives_users',
            ['collation' => 'utf8mb4_unicode_ci']);
        $table->addColumn('initiative_id', 'integer', [
            'null' => false,
        ])->addColumn('user_id', 'integer', [
            'null' => false,
        ])->addColumn('score', 'integer', [
            'null' => false,
        ])->addColumn('state', 'string', [
            'limit' => MysqlAdapter::TEXT_TINY,
            'null' => true,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeUserAnswersMakeNullableAnswerId extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'user_answers');
        $table->changeColumn('answer_id', 'integer', [
            'default' => null,
            'null' => true
        ])->update();
    }
}

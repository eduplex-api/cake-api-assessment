<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeQuestionnairesAddSelectorText extends AbstractMigration
{
    public function change()
    {
        $questionnaires = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires');
        $questionnaires->addColumn('selector_text', 'string', [
            'null' => true,
            'after' => 'slug'
        ])->update();
    }
}

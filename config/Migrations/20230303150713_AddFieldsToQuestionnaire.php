<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AddFieldsToQuestionnaire extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires');
        $table->addColumn('signup_description', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'related_questionnaire_id'
        ])->addColumn('pdf_description_1', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'signup_description'
        ])->addColumn('pdf_description_2', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'pdf_description_1'
        ])->addColumn('pdf_image', 'string', [
            'null' => true,
            'after' => 'pdf_description_2'
        ])->addColumn('level_1', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'pdf_image'
        ])->addColumn('level_2', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'level_1'
        ])->addColumn('level_3', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'level_2'
        ])->addColumn('level_4', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'level_3'
        ]);
        $table->update();
    }
}

<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeInitiativesDropPoints extends AbstractMigration
{
    public function change()
    {
        $initiatives = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives');
        $initiatives->removeColumn('points')->update();
    }
}

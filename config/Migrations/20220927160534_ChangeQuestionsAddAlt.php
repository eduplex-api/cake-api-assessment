<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class ChangeQuestionsAddAlt extends AbstractMigration
{
    public function change()
    {
        $answers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questions');
        $answers->addColumn('alt1', 'string', [
            'null' => true,
            'after' => 'position'
        ])->addColumn('alt2', 'string', [
            'null' => true,
            'after' => 'alt1'
        ])->update();
    }
}

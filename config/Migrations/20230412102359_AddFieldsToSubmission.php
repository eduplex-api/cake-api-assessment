<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddFieldsToSubmission extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'submissions');
        $table->addColumn('occupation', 'string', [
            'null' => true,
            'after' => 'job_position'
        ])->addColumn('working_times', 'string', [
            'null' => true,
            'after' => 'occupation'
        ])->addColumn('home_office', 'boolean', [
            'null' => true,
            'after' => 'working_times'
        ]);
        $table->update();
    }
}

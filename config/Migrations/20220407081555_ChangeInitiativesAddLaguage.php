<?php
declare(strict_types=1);

use App\Lib\Consts\LanguagesISO6393 as Languages;
use Cake\Database\Query;
use Migrations\AbstractMigration;

class ChangeInitiativesAddLaguage extends AbstractMigration
{
    public function change()
    {
        $initiatives = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives');
        $initiatives->addColumn('language', 'string', [
            'limit' => 10,
            'null' => true,
            'after' => 'description'
        ])->update();

        $builder = $this->getQueryBuilder();
        $builder
            ->update(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives')
            ->set('language', Languages::ENG)
            ->where(['language is NULL'])
            ->execute();
    }

    public function getQueryBuilder(): Query
    {
        // this function can be totally removed after upgrade
        // getQueryBuilder() will throw deprecated until we upgrade to migrations 4.x and cake 5.x
        /** @var \Migrations\CakeAdapter $addapter */
        $addapter = $this->getAdapter();
        // return $addapter->getQueryBuilder();
        return $addapter->getCakeConnection()->updateQuery();
    }
}

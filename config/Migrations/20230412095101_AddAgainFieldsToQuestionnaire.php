<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AddAgainFieldsToQuestionnaire extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires');
        $table->addColumn('checkbox_label', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'single_submit'
        ])->addColumn('email_label', 'string', [
            'null' => true,
            'after' => 'checkbox_label'
        ])->addColumn('question_info', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'email_label'
        ])->addColumn('user_info', 'text', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_REGULAR,
            'after' => 'question_info'
        ]);
        $table->update();
    }
}

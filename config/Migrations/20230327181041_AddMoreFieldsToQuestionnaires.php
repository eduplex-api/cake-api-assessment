<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AddMoreFieldsToQuestionnaires extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'questionnaires');
        $table->addColumn('pdf_title', 'string', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_SMALL,
            'after' => 'pdf_image'
        ])->addColumn('level_1_title', 'string', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_SMALL,
            'after' => 'level_4'
        ])->addColumn('level_2_title', 'string', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_SMALL,
            'after' => 'level_1_title'
        ])->addColumn('level_3_title', 'string', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_SMALL,
            'after' => 'level_2_title'
        ])->addColumn('level_4_title', 'string', [
            'null' => true,
            'limit' => MysqlAdapter::TEXT_SMALL,
            'after' => 'level_3_title'
        ]);
        $table->update();
    }
}

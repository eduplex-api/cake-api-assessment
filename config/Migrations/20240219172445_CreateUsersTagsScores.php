<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateUsersTagsScores extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'users_tags_scores',
            ['collation' => 'utf8mb4_unicode_ci']);
        $table->addColumn('user_id', 'integer', [
            'null' => false,
        ])->addColumn('tag', 'string', [
            'null' => false,
        ])->addColumn('score', 'integer', [
            'null' => true,
        ])->addColumn('max_score', 'integer', [
            'null' => true,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex(['user_id', 'tag']);
        $table->create();
    }
}

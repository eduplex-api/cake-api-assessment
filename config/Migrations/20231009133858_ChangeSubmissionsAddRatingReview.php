<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class ChangeSubmissionsAddRatingReview extends AbstractMigration
{
    public function change(): void
    {
        $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'submissions')
            ->addColumn('rating', 'integer', [
                'after' => 'questionnaire_id',
                'limit' => MysqlAdapter::INT_SMALL,
                'null' => true
            ])
            ->addColumn('review', 'text', [
                'after' => 'questionnaire_id',
                'limit' => MysqlAdapter::TEXT_MEDIUM,
                'null' => true
            ])
            ->update();
    }
}

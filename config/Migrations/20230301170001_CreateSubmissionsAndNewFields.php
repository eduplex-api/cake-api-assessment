<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateSubmissionsAndNewFields extends AbstractMigration
{
    public function change()
    {
        $submissions = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'submissions');
        $submissions->addColumn('user_id', 'integer', [
            'null' => false,
        ])->addColumn('job_position', 'string', [
            'null' => true,
        ])->addColumn('location', 'string', [
            'null' => true,
        ])->addColumn('questionnaire_id', 'integer', [
            'null' => false,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $submissions->addIndex('questionnaire_id')->create();

        $initiativesUsers = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives_users');
        $initiativesUsers->addColumn('submission_id', 'integer', [
            'null' => true,
            'after' => 'manager_id'
        ])->addIndex('submission_id')->update();
    }
}

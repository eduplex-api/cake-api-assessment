<?php
declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateAnswers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'answers',
            ['collation' => 'utf8mb4_unicode_ci']);
        $table->addColumn('question_id', 'integer', [
            'null' => false,
        ])->addColumn('answer', 'string', [
            'limit' => MysqlAdapter::TEXT_TINY,
            'null' => false,
        ])->addColumn('weight', 'integer', [
            'limit' => 10,
            'null' => false,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addIndex(['question_id']);
        $table->create();
    }
}

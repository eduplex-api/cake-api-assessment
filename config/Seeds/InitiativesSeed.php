<?php
declare(strict_types=1);

use App\Lib\Consts\LanguagesISO6393 as Languages;
use Migrations\AbstractSeed;

class InitiativesSeed extends AbstractSeed
{

    const INITIATIVE_ID = 1;

    public function run(): void
    {
        $now = date('Y-m-d H:i:00');
        $data = [
            [
                'id' => self::INITIATIVE_ID,
                'title' => 'SCRUM 101 Course',
                'description' => 'Description of the course',
                'language' => Languages::ENG,
                'created' => $now,
                'modified' => $now,
                'deleted' => null,
            ]
        ];

        $table = $this->table(\Assessment\AssessmentPlugin::getTablePrefix() . 'initiatives');
        $table->insert($data)->save();
    }
}
